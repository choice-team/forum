<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionLikeMapper extends Model
{
    use SoftDeletes, LogsActivity;
    protected static $logFillable = true;
    protected $table = 'user_forum_like_mapper';

    public $fillable = ['user_id','question_id','thread_id','like_status'];
}

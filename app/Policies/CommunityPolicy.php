<?php
namespace App\Policies;
 
use App\Users;
use App\ForumTopics;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Auth\Access\HandlesAuthorization;
 
class CommunityPolicy
{
  use HandlesAuthorization;
 
  /**
   * Determine whether the user can view the post.
   *
   * @param  \App\Users  $user
   * @param  \App\ForumTopics  $top
   * @return mixed
   */
  public function list(Users $user, ForumTopics $top)
  {
    $data = $this->getPermissionData($user->id);
    if($data->contain('role_id',1))
        return TRUE;

    if($data->contain('slug',$top->topic_slug.'-list'))
        return TRUE;
    return FALSE;
  }
 
  /**
   * Determine whether the user has create permissions.
   *
   * @param  \App\Users  $user
   * @param  \App\Taxonomy  $top
   * @return mixed
   */
  public function create(Users $user, ForumTopics $top)
  {
    $data = $this->getPermissionData($user->id);
    if($data->contains('role_id',1))
      return TRUE;

    if($data->contains('slug',$top->topic_slug.'-create'))
      return TRUE;
    return FALSE;
  }
 
  /**
   * Determine whether the user has update permission.
   *
   * @param  \App\Users  $user
   * @param  \App\Taxonomy  $top
   * @return mixed
   */
  public function update(Users $user, ForumTopics $top)
  {
    $data = $this->getPermissionData($user->id);
    if($data->contains('role_id',1))
      return TRUE;

    if($data->contains('slug',$top->topic_slug.'-update'))
      return TRUE;
    return FALSE;
  }
 
  /**
   * Determine whether the user deactivate permission.
   *
   * @param  \App\Users  $user
   * @param  \App\ForumTopics  $top
   * @return mixed
   */
  public function deactivate(Users $user, ForumTopics $top)
  {
    $data = $this->getPermissionData($user->id);
    
    if($data->contains('role_id',1))
      return TRUE;

    if($data->contains('slug',$top->topic_slug.'-deactivate'))
      return TRUE;
    return FALSE;
  }

 /**
   * Determine whether the user has view permission.
   *
   * @param  \App\Users  $user
   * @param  \App\Taxonomy  $top
   * @return mixed
   */
  public function view(Users $user, ForumTopics $top)
  {
    $data = $this->getPermissionData($user->id);
    if($data->contains('role_id',1))
      return TRUE;

    if($data->contains('slug',$top->topic_slug.'-list'))
      return TRUE;
 
    return FALSE;
  }
   /**
   * Determine whether the user has approve permission.
   *
   * @param  \App\Users  $user
   * @param  \App\Taxonomy  $top
   * @return mixed
   */
  public function approve(Users $user, ForumTopics $top)
  {
    $data = $this->getPermissionData($user->id);
    if($data->contains('role_id',1))
      return TRUE;

    if($data->contains('slug',$top->topic_slug.'-approve'))
      return TRUE;
    return FALSE;
  }

   /**
   * Determine whether the user has reject permission.
   *
   * @param  \App\Users  $user
   * @param  \App\Taxonomy  $top
   * @return mixed
   */
  public function reject(Users $user, ForumTopics $top)
  {
    $data = $this->getPermissionData($user->id);
    
    if($data->contains('role_id',1))
      return TRUE;

    if($data->contains('slug',$top->topic_slug.'-reject'))
      return TRUE;
    return FALSE;
  }


  /** 
   * get the role and permission belongs to given users
   * @param [Number] userId
  */ 
  public function getPermissionData($userId) {
  
      return DB::table('users_roles')->select('users_roles.role_id','permissions.slug')
        ->leftjoin('roles_permissions','roles_permissions.role_id','=','users_roles.role_id')
        ->leftjoin('permissions','permissions.id','=','roles_permissions.permission_id')
        ->where('user_id',$userId)->get();

  }
}
<?php

namespace App;

use App\ForumQuestions;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumTopics extends Model
{
    use SoftDeletes, LogsActivity;
    protected static $logFillable = true;
    protected $table = 'forum_topics';

    public $fillable = ['topic_name','topic_description','topic_slug','topic_image','user_id','created_at','updated_at', 'deleted_at'];
    
     /**
     * Get the comments for the blog post.
     */
    public function questions()
    {
        return $this->hasMany(ForumQuestions::class,'topic_id','topic_name');
    }
}

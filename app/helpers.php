<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use League\Flysystem\FilesystemInterface;

    /**
     * Reform  store media on server
     *
     * @param array $media
     *
     * @return json
     */
    

      function likedforum_store_csv($media, $filesystem,$source=NULL) {

        $data = [];
        $count = 0;
        $extAllowedImage = ['csv'];
            $extension ='csv';
            if(in_array(strtolower($extension),$extAllowedImage)) {
                $date = new \DateTime(null, new \DateTimeZone('Asia/Kolkata'));
                $current_date = $date->getTimestamp();
                $fileName =  'liked_forum_'.$current_date.".".$extension;
                $stream = fopen($media, 'r+');
                try {
                    $data =  $filesystem->writeStream('/uploads/recommendation/forum/'.$fileName,$stream);
                } catch(Exception $e) {
                    return ($e->getMessage());
                }
            } 
        return $fileName;
    }

     function unlikedforum_store_csv($media, $filesystem,$source=NULL) {

        $data = [];
        $count = 0;
        $extAllowedImage = ['csv'];
            $extension ='csv';
            if(in_array(strtolower($extension),$extAllowedImage)) {
                $date = new \DateTime(null, new \DateTimeZone('Asia/Kolkata'));
                $current_date = $date->getTimestamp();
                $fileName =  'unliked_forum'.$current_date.".".$extension;
                $stream = fopen($media, 'r+');
                try {
                    $data =  $filesystem->writeStream('/uploads/recommendation/forum/'.$fileName,$stream);
                } catch(Exception $e) {
                    return ($e->getMessage());
                }
            } 
        return $fileName;
    }

?>
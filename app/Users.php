<?php

namespace App;

use App\Models\Role;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Users extends Model implements AuthenticatableContract, AuthorizableContract
{
    use  Authenticatable, Authorizable, SoftDeletes, LogsActivity;
    protected static $logFillable = true;
    protected $table = 'users';
    protected $fillable = ['user_type','user_code', 'first_name','last_name','email','password','remember_token','mobile','mentorship_status','role_id','created_at','updated_at','deleted_at'];


    public function userProfile(){
        return $this->hasOne('App\UsersProfile','user_id','id');
         // return $this->hasOne('App\UsersProfile');
    }
    public function companyTeamMember(){
        return $this->hasMany('App\CompanyDirectors','user_id','id');
    }
    public function companyFiles(){
        return $this->hasMany('App\CompanyDocuments','user_id','id');
    }
    public function entrepreneur(){
        return $this->hasOne('App\Enterpreneur','user_id','id');
    }
    public function qualification(){
        return $this->hasMany('App\UserQualification','user_id','id')->select('id', 'qualification_name');;
    }
    public function companyAwards(){
        return $this->hasMany('App\CompanyAwards','user_id','id');
    }
    public function skills(){
        return $this->hasMany('App\MentorSkill','user_id','id');
    }
    
    protected function getAllRoles(array $roles) {
		return Role::whereIn('id',$roles[0])->get();
    }
    
	public function withdrawlRolesTo( ... $roles) {
      
        $roles = $this->getAllRoles($roles);
        
		$this->roles()->detach($roles);
		return $this;
    }
    
    public function hasAccess(array $permissions) {
        foreach($this->roles as $role) {
            if($role->hasAccess($permissions)) {
                return true;
            } 
        }
        return false;
    }
}

<?php

namespace App;

use App\ForumTopics;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumQuestions extends Model
{
    use SoftDeletes, HasSlug, LogsActivity;
    protected static $logFillable = true;
    protected $table = 'forum_questions';

    public $fillable = ['forum_id','topic_id','user_id','forum_question','description','forum_question_slug','likes','dislikes','posted_date','publish_date','status'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('forum_question')
            ->saveSlugsTo('forum_question_slug')
            ->slugsShouldBeNoLongerThan(100);
    }

    public function topic()
    {
    	return $this->belongsTo(ForumTopics::class,'topic_id','topic_name');
    }

       /**
     * Get the comments for the blog post.
     */
    public function thread()
    {
        return $this->hasMany(ForumThread::class,'question_id');
    }

}

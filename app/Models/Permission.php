<?php

namespace App\Models;
use App\Models\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{

    use SoftDeletes;

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'slug','module_id','operation_id','created_at','updated_at','deleted_at'
    ];

    public function roles() {
        return $this->belongsToMany(Role::class,'roles_permissions','role_id','permission_id')->withTimestamps();
    }

    public function modules() {
        return $this->belongsTo(Module::class);
    }
}

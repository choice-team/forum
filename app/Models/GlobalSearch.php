<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class GlobalSearch extends Model
{
	use SoftDeletes,LogsActivity;
    protected static $logFillable = true;
    public $timestamps = false;

    protected $table = 'search_activitylog';

    public $fillable = ['category','search_keyword','search_results','user_id','created_at','updated_at', 'deleted_at'];

}

<?php
namespace App\Models;

use App\Users;
use Illuminate\Database\Eloquent\Model;
use App\Permissions\HasPermissionsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasPermissionsTrait,SoftDeletes;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug','created_by','type','updated_by','updated_at','deleted_at','module_id'
    ];

    public function permissions() {
        return $this->belongsToMany(Permission::class,'roles_permissions','role_id','permission_id');
    }
    

    /*
    * user role relationship
    */
    public function users() {
        return $this->belongsToMany(Users::class,'users_roles','role_id','user_id');
    }

    public function hasAccess(array $permissions) {
        
        foreach($permissions as $permission) {
            if($this->hasPermission($permission)) {
                return true;
            } 
        }
        return false;
    }

    public function hasPermission(string $permission) {
        $permissions = $this->permissions;
        // $permissions = DB::table('role_permission')->join('permissions','permissions.id','=','role_permission.permission_id')->where('role_id',$this->id)->pluck('permissions.slug');
        foreach($permissions as $value) {
             if($value['slug'] == $permission) {
                 return true;
             }
         }
         return false;
     }
  
}

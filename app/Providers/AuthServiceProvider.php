<?php

namespace App\Providers;

use App\Users;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use App\Policies\CommunityPolicy;
use App\ForumTopics;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected $policies = [
        'App\Model' => 'App\Policies\CommunityPolicy',
        ForumTopics::class => CommunityPolicy::class
      ];


    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        $this->app['auth']->viaRequest('api', function ($request) {

            if ($request->header('Authorization')) {
                $authorization = explode(' ',$request->header('Authorization'));
            if(isset($authorization[1])){
            $token =explode('~~', base64_decode($authorization[1]));
            $user = Users::where('api_token', $token[0])->first();
            if(!empty($user)){
                $request->request->add(['user_id' => $user->id]);
            }
            return $user;
            }

        }

        });

        $this->registerPolicies();
    }


    public function registerPolicies() {
        Gate::define('list', 'App\Policies\CommunityPolicy@list');
        Gate::define('create', 'App\Policies\CommunityPolicy@create');
        Gate::define('update', 'App\Policies\CommunityPolicy@update');
        Gate::define('deactivate', 'App\Policies\CommunityPolicy@deactivate');
        Gate::define('approve', 'App\Policies\CommunityPolicy@approve');
        Gate::define('view', 'App\Policies\CommunityPolicy@view');
        Gate::define('reject', 'App\Policies\CommunityPolicy@reject');
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
/**
* Custom CORS and HTTP REST JSON Response handler as per Choice Techlab REST API RESPONSE FORMAT standard
**/
class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $headers = [
            'Access-Control-Allow-Origin'      => '*',
            'Access-Control-Allow-Methods'     => 'POST, GET, OPTIONS, PUT, PATCH, DELETE',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Max-Age'           => '86400',
            'Access-Control-Allow-Headers'     => 'Content-Type, Authorization, X-Requested-With'
        ];
        if($request->isMethod('OPTIONS')){

            return response()->json('{"method":"OPTIONS"}', 200, $headers);
        }
        $response = $next($request);
        $body = array(
            'status_code'=> (int)($response->status()/10).'0'.($response->status()%100),
            'message'=>(($response->status()!='200')?"error":"success"),
            'body'=> json_decode($response->content())
        );

        return response()->json($body, $response->status(), $headers);
    }
}
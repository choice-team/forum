<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Log;
use Exception;
use Rap2hpoutre\FastExcel\FastExcel;
use App\QuestionLikeMapper;

class ForumDataJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable,SerializesModels;

    protected $request;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle()
    {
        $user_data = QuestionLikeMapper::select('question_id as forum_id','forum_topics.topic_slug',DB::raw('GROUP_CONCAT(user_forum_like_mapper.user_id) ids_of_liked_users'),DB::raw('count(user_forum_like_mapper.user_id) as number_of_likes'))
        ->join('forum_questions','user_forum_like_mapper.question_id','=','forum_questions.id')
        ->join('users','forum_questions.user_id','=','users.id')
        ->join('forum_topics','forum_questions.topic_id','=','forum_topics.id')
        ->groupBy('question_id')
        ->get()->toArray();

        $fileName = 'forum_details'.time().'.'."csv";
        $path = 'recommendation/forum/'.date('Ymd');
        $filePath = $path.'/'.$fileName;
        
        if (!file_exists(storage_path($path))) {
            mkdir(storage_path($path), 0777, true);
        }
        unset($path,$fileName);
        $result = (new FastExcel($user_data))->export(storage_path($filePath));
        if ($result) {
            Storage::disk('local')->put($filePath, file_get_contents(storage_path($filePath)));
            return $result;
        }
    }
    
    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {

    }
}

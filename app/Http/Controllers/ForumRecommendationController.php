<?php
/**
 * WEP
 *
 * PHP Version 7.1
 *
 * @category RecommendationController
 * @author    Choice Tech Lab <contact@choicetechlab.com>
 * @copyright 2017-2018 Choice Tech Lab (https://choicetechlab.com)
 * @license   https://choicetechlab.com/licenses/ctl-license.php CTL General Public License
 * @version  1.0.0
 * @package App\Http\Controllers\RecommendationController
 * @link      https://choicetechlab.com/
 */

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Queue;
use Rap2hpoutre\FastExcel\FastExcel;
use App\QuestionLikeMapper;
use App\Users;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\ForumQuestions;
use App\ForumThread;
use App\Validators\MathCaptcha;
use League\Flysystem\FilesystemInterface;
use Illuminate\Http\JsonResponse;



class ForumRecommendationController extends Controller
{
	public function __construct()
	{
		if(isset(Auth::user()->id))
			$this->user_id = Auth::user()->id;
    }
     /**
     * @api {get} /api/community/forum-recommendation Forum Recommendation
     * @apiName Get all forum details and dump data in CSV and return path of CSV
     * @apiGroup Recommendation
     * @apiSuccessExample Success-Response-2000:
         HTTP/1.1 2000 OK
      {
        "status_code": "2000",
        "message": "success",
        "body": {
            "status_code": "200",
            "message": "Forum Details fetched successfully!!",
            "body": "recommendation/forum/20200529/forum_details1590767314.csv"
        }
    }

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
     public function likedForum(Request $request,FilesystemInterface $filesystem) {

     	$request->request->add(['request_token' => env('TOKEN')]);
       $this->validate($request, ['api_token' => 'required|same:request_token']);

       $forum_data = DB::select(DB::raw('select forum_questions.id as forum_id,`categories`.`category_name` as `topic_slug`, GROUP_CONCAT(COALESCE(user_forum_like_mapper.user_id,"--") SEPARATOR "|") as ids_of_liked_users, count(user_forum_like_mapper.user_id) as number_of_likes,`forum_questions`.`created_at`  FROM forum_questions  left join user_forum_like_mapper on user_forum_like_mapper.question_id = forum_questions.id left join users on users.id = user_forum_like_mapper.user_id = users.id left join `categories` on `categories`.`id` = `forum_questions`.`topic_id` where forum_questions.deleted_at is null and forum_questions.status ="Approved" and categories.deleted_at is null and user_forum_like_mapper.deleted_at is null group by forum_questions.id'));


       if(!empty($forum_data))
       {
           $fileName = 'forum_'.time().'.'."csv";
           $result = (new FastExcel($forum_data))->export($fileName);
           $filename =   likedforum_store_csv($result,$filesystem);

           if ($filename) {
            $data['status_code'] = '200';
            $data['api_token'] = env('TOKEN');
            $data['message'] = "Forum Details fetched successfully!!";
            $data['body'] = env('FRONTEND_URL').'/uploads/recommendation/forum/'.$filename;
            return $data;
        }
    } else {
       $data['status_code'] = '200';
       $data['api_token'] = env('TOKEN');
       $data['message'] = "Data Not found";
       $data['body'] = [];
       return $data;
   }
}

public function unLikedForum(Request $request,FilesystemInterface $filesystem) {

    $request->request->add(['request_token' => env('TOKEN')]);
    $this->validate($request, ['api_token' => 'required|same:request_token']);

    $forum_data = DB::select(DB::raw('SELECT forum_questions.id as forum_id, forum_questions.created_at, categories.category_name as categories FROM `forum_questions`  join categories on categories.id = forum_questions.topic_id WHERE forum_questions.`id` NOT IN ( select question_id from user_forum_like_mapper where question_id is not null)'));

    if(!empty($forum_data))
    {
       $fileName = 'forum_'.time().'.'."csv";
       $result = (new FastExcel($forum_data))->export($fileName);
       $filename =   unlikedforum_store_csv($result,$filesystem);
       if ($filename) {
        $data['status_code'] = '200';
        $data['api_token'] = env('TOKEN');
        $data['message'] = "Forum Details fetched successfully!!";
        $data['body'] = env('FRONTEND_URL').'/uploads/recommendation/forum/'.$filename;
        return $data;
    }
} else {
   $data['status_code'] = '200';
   $data['api_token'] = env('TOKEN');
   $data['message'] = "Data Not found";
   $data['body'] = [];
   return $data;
}
}



     /**
     * @api {post} /api/community/forum-recommendation-id Recommended Forums
     * @apiName Call the Recommendation API and get all the forum ids and fetech all the details
     * @apiGroup Recommendation
     * @apiParam {search} search search forum question
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
  {
    "status_code": "2000",
    "message": "success",
    "body": {
        "current_page": 1,
        "data": [
            {
                "question_id": 69,
                "id": 69,
                "forum_id": null,
                "topic_id": 10,
                "forum_question": "Mahila gruh udyog Kendra",
                "forum_question_slug": "mahila-gruh-udyog-kendra",
                "description": "Hi, i want to know how to start/register mahila gruh udyog kendra & what are the pre-requisites for the same along with benefits. ",
                "user_id": null,
                "likes": 0,
                "dislikes": 0,
                "posted_date": "0000-00-00",
                "publish_date": null,
                "status": "Approved",
                "created_at": "2019-09-11 22:42:40",
                "updated_at": "2019-09-11 22:42:40",
                "deleted_at": null,
                "first_name": null,
                "last_name": null,
                "user_type": null,
                "profile_pic": null,
                "display_name": null,
                "gender": null,
                "user_like_status": 1
            },
            {
                "question_id": 87,
                "id": 87,
                "forum_id": null,
                "topic_id": 10,
                "forum_question": "How can i start my business",
                "forum_question_slug": "how-can-i-start-my-business-7",
                "description": "How can i start my business",
                "user_id": null,
                "likes": 0,
                "dislikes": 0,
                "posted_date": "0000-00-00",
                "publish_date": null,
                "status": "Approved",
                "created_at": "2019-09-18 16:13:57",
                "updated_at": "2020-05-29 09:59:40",
                "deleted_at": null,
                "first_name": null,
                "last_name": null,
                "user_type": null,
                "profile_pic": null,
                "display_name": null,
                "gender": null,
                "user_like_status": 1
            }
        ],
        "first_page_url": "http://wepcommunity.choicetechlab.com/api/community/forum-recommendation-id?page=1",
        "from": 1,
        "last_page": 64,
        "last_page_url": "http://wepcommunity.choicetechlab.com/api/community/forum-recommendation-id?page=64",
        "next_page_url": "http://wepcommunity.choicetechlab.com/api/community/forum-recommendation-id?page=2",
        "path": "http://wepcommunity.choicetechlab.com/api/community/forum-recommendation-id",
        "per_page": 15,
        "prev_page_url": null,
        "to": 3,
        "total": 956
    }
}

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
    //  public function getForumRecommendedId(Request $request) {
    //     $client = new Client(); //GuzzleHttp\Client
    //     $res = $client->post(env('RECOMMEND'), [
    //     	'json' => [
    //     		'user_id' => $this->user_id
    //     	]
    //     ]);

    //     if($res->getStatusCode() == '400'){
    //     	return new JsonResponse(['message'=>"Something went wrong!! Please check Machine Learning End point"],400);
    //     }

    //     $response = json_decode($res->getBody());

    //     if($res->getStatusCode() == '200' && $response->body->forum != '') {

    //     	$forumdata =  QuestionLikeMapper::select(DB::raw('DISTINCT(user_forum_like_mapper.question_id)'),'forum_questions.*','users.first_name','users.last_name','users.user_slug as user_id', 'users.user_type','user_profile.profile_pic','users.display_name','user_profile.gender','user_forum_like_mapper.like_status as user_like_status')
    //     	->leftjoin('forum_questions', function($join) {
    //     		$join->on('user_forum_like_mapper.question_id','=','forum_questions.id');
    //            // $join->where('user_forum_like_mapper.user_id', '=',409);
    //     	})
    //     	->leftjoin('users' ,'users.id', '=' ,'forum_questions.user_id')
    //     	->leftjoin('forum_topics','forum_questions.topic_id' ,'=', 'forum_topics.id') 
    //     	->leftjoin('categories','forum_questions.topic_id', '=','categories.id')
    //     	->leftjoin('user_profile', 'users.id' ,'=' ,'user_profile.user_id')
    //     	->leftjoin('entrepreneur_interest_area','entrepreneur_interest_area.interest_area_id', '=','categories.id')
    //     	//->whereIn('user_forum_like_mapper.question_id',$response->body->forum)
    //     	->where(['forum_questions.status' => 'Approved','forum_questions.deleted_at' => NULL])
    //         ->orderBy('forum_questions.id','desc');

    //     	if(isset($request->search) && !empty($request->search))  {
    //     		$forumdata = $forumdata->where('forum_questions.forum_question','like','%'.$request->search.'%');
    //     	}

    //     	return  $data = $forumdata->paginate();
    //     } else {
    //     	return new JsonResponse(['message'=>"Data not found"],200);
    //     }
    // }

     public function getForumRecommendedId(Request $request)
     {
      $user_id = $this->user_id;

           $client = new Client(); //GuzzleHttp\Client
           $res = $client->post(env('RECOMMEND'), [
            'json' => [
                'user_id' => $this->user_id,
                'api_token' => env('TOKEN')
            ]
        ]);

           if($res->getStatusCode() == '400'){
            return new JsonResponse(['message'=>"Something went wrong!! Please check Machine Learning End point"],400);
        }

        $response = json_decode($res->getBody());
        $data =[];
        if($res->getStatusCode() == '200' && $response->body->forum != '') {
           $question_list = DB::table('forum_questions')->select(DB::raw('DISTINCT (forum_questions.id)','forum_questions.id'), 'forum_questions.id','forum_questions.forum_question', 'categories.category_name as topic_name', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','users.display_name','user_profile.gender','user_forum_like_mapper.like_status')
           ->leftjoin('users','users.id','=','forum_questions.user_id')
           ->leftjoin('user_profile','users.id','=','user_profile.user_id')
           //->leftjoin('forum','forum.id','=','forum_questions.forum_id')
           ->leftjoin('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
           ->leftjoin('categories','categories.id','=','forum_topics.topic_name')
           ->leftjoin('user_forum_like_mapper','user_forum_like_mapper.question_id','=','forum_questions.id')
           ->leftjoin('entrepreneur_interest_area','entrepreneur_interest_area.interest_area_id' ,'=','categories.id')
           ->whereIn('forum_questions.id',$response->body->forum)
           ->where(['forum_questions.deleted_at'=>NULL])
           ->where(['forum_questions.status'=>'Approved'])
           //->where('entrepreneur_interest_area.user_id',$user_id)
           ->groupBy('forum_questions.id')
           //->orderBy(DB::raw('FIELD($response->body->forum)'))
           ->orderBy('forum_questions.id','desc')
           ->get();


           $questions = array();
           if (isset($question_list) && count($question_list) > 0) {
              foreach ($question_list as $key1 => $value1) {
                $data[$key1]['question_id'] = $value1->id;
                $data[$key1]['question'] = $value1->forum_question;
                $data[$key1]['topic_name'] = $value1->topic_name;
                $data[$key1]['publish_date'] = $value1->publish_date;
                $data[$key1]['likes'] = QuestionLikeMapper::where([
                  'question_id' => $value1->id,
                  'like_status' => 1,
              ])->count();

                $data[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$data[$key1]['question_id'])->count();
       // $questions[$key1]['description'] = substr($value1->description,0,200);
                $data[$key1]['description'] = $value1->description;
                $data[$key1]['slug'] = $value1->forum_question_slug;
                $data[$key1]['first_name'] = isset($value1->first_name)?$value1->first_name:"";
                $data[$key1]['last_name'] = isset($value1->last_name)?$value1->last_name:"";
                $data[$key1]['gender'] = isset($value1->gender)?$value1->gender:"";
                $data[$key1]['user_id'] = isset($value1->user_id)?$value1->user_id:"";
                $data[$key1]['user_type'] = isset($value1->user_type)?$value1->user_type:"";
                $data[$key1]['display_name'] = isset($value1->display_name)?$value1->display_name:"";
                $data[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:"";
                $data[$key1]['entreprise_name'] = isset($value1->entreprise_name)?$value1->entreprise_name:"";
                $data[$key1]['like_status'] = isset($value1->like_status)?$value1->like_status:"";
                if($value1->user_id && $value1->user_type && $value1->user_type == 'partner') {
                  $slug = DB::table('partner_profile')->where('user_id',$value1->user_id)->first();
                  $data[$key1]['partner_slug'] = $slug ? $slug->slug:'';
                  $data[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:(DB::table('partner_profile')->where('slug', $value1->user_id)->first()?DB::table('partner_profile')->where('slug', $value1->user_id)->first()->logo:'');
              }
          }

          if (count($data) > 0) {
            return $data['data'] = $data;
        }
    } else {
      return $data;
  }
} else{
 return $data; 
}
}



public function getLikedForums(Request $request)
{

 $request->request->add(['request_token' => env('TOKEN')]);
 $this->validate($request, ['api_token' => 'required|same:request_token',
    'user_id' => 'required' ]);

 $user_id =  Users::find($request->user_id);
 if(!empty($user_id)){
     $forumdata =  QuestionLikeMapper::select(DB::raw('DISTINCT(user_forum_like_mapper.question_id) as forum_id') )
     ->join('forum_questions' ,'user_forum_like_mapper.question_id','=','forum_questions.id')
     ->where(['forum_questions.status' => 'Approved','user_forum_like_mapper.like_status' => 1, 'forum_questions.deleted_at'=> null,'user_forum_like_mapper.deleted_at'=>null ])
     ->where('user_forum_like_mapper.question_id', '!=' ,null)
     ->orderBy('user_forum_like_mapper.question_id','desc')
     ->get()->toArray();
     $forum=[];
     if(!empty($forumdata)){
      foreach ($forumdata as $key => $value) {
       $forum['forum_id'][$key] = $value['forum_id'];
   } 
}
return $forum;
} else {
   $data['status_code'] = '200';
   $data['api_token'] = env('TOKEN');
   $data['message'] = "User ID Not Found";
   $data['body'] = [];
   return $data;
}

}

}
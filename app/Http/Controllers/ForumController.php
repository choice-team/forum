<?php

namespace App\Http\Controllers;

use DB;
use App\Forum;
use App\Users;
use Exception;
use App\ForumThread;
use App\ForumTopics;
use App\Services\Slug;
use App\ForumQuestions;
use App\QuestionLikeMapper;
use Illuminate\Http\Request;
// use App\PartnerProfile;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
/**
 * Home file for action on Forums.
 * PHP version 7
 *
 * @category PHP
 * @package  Forums API for Niti Aayog
 * @author   ChoiceTechLab <contact@choicetechlab.com>
 * @version  1.0
 */

class ForumController extends Controller
{
  protected $slugName;
  private $user_id;

  public function __construct(Slug $slug)
  {
   $this->slugName = $slug;
   $this->user_id = Auth::user()->id;

 }

    /**
     * Show the list of Forum.
     * Set the Limit and Offset from GET request
     * @return \Illuminate\Http\Response
     */

    public function getForumListing(Request $request)
    {


     if($request->has('limit')) {
      $limit = $request->get('limit');
    }else{
      $limit = 10;
    }
    if($request->has('offset')) {
      $offset = $request->get('offset');
    }else{
      $offset = 0;
    }

    $forum_list = Forum::select('id', 'forum_name', 'forum_description', 'forum_image', 'user_id')
    ->where(['deleted_at'=>NULL])
    ->limit($limit)
    ->offset($offset)
    ->get();

    if (count($forum_list) > 0) {
      $data['status_code'] = '200';
      $data['message'] = "success";
      $data['count'] = count($forum_list);
      $data['body'] = $forum_list;
      // $data['last_id'] = $insert_blog->id;
      return $data;
    }
  }

  /**
     * Create Forum.
     * Set the Parameters in POST request
     * forum_name : Long Text
     * forum_description : Long Text
     * user_id : Current Logged in User ID
     * forum_image : Path of the Image which has been uploaded
     * @return \Illuminate\Http\Response
     */

  public function createForum(Request $request)
  {

    $requestData = $request->all();
    $this->validate($request,[
      'forum_name'   => 'required',
      'forum_description'   => 'required',
      'user_id'         => 'required'
    ]);

    $insert_forum = Forum::create($requestData);

    if ($insert_forum) {
      $data['status_code'] = '200';
      $data['message'] = "Inserted Successfully";
      $data['last_id'] = $insert_forum->id;
    }
    return $data;
  }

  /**
     * Show the Single Forum.
     * Set ID of the Forum to get the Particular Forum in GET request.
     * @return \Illuminate\Http\Response
     */

  public function viewForum($id)
  {
    $forum = Forum::findOrFail($id);
    if ($forum) {
      return $forum;
    }
  }

  public function updateForum(Request $request,$id)
  {
    $requestData = $request->all();
    $this->validate($request,[
      'forum_name'   => 'required',
      'forum_description'   => 'required',
      'user_id'         => 'required'
    ]);

    $forum = Forum::findOrFail($id);
    $forum_update = $forum->update($requestData);

    if ($forum_update) {
      $data['status_code'] = '200';
      $data['message'] = "Updated successfully";
      return $data;
    }
  }


    /**
   * @api {post} {base_url}/api/community/forum/{id} Delete
   * @apiName  Delete forum question
   * @apiGroup My Forum
   * @apiDescription To delete forum question
   * @apiHeader {String} Authorization api token key.
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
     {
      "status_code":"2000",
      "message":"success",
      "body":[

      ]
    }

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */

  /**
   * This function provide the list of all taxes.
   *
   * @param mixed[] $request Request structure to get the post data for pagination like limit and offset.
   *
   * @var int $limit Should contain a number for limit of record by default it is 10.
   * @var int $offset Should contain a number for offset of record by default it is 0.
   *
   * @return array of objects.
   *
   */
  public function deleteForum($id)
  {
    $delete_data = ForumQuestions::where('id',$id)->delete();
    if ($delete_data) {
      return new JsonResponse([],200);
    }
  }

  public function getTopicListing(Request $request)
  {
    if($request->has('limit')) {
      $limit = $request->get('limit');
    }else{
      $limit = 10;
    }
    if($request->has('offset')) {
      $offset = $request->get('offset');
    }else{
      $offset = 0;
    }
    $topic_list = ForumTopics::select('forum_topics.id', 'categories.category_name as topic_name','topic_slug', 'topic_description', 'topic_image', 'user_id','users.first_name','users.last_name')
    ->join('users','forum_topics.user_id','=','users.id')
    ->join('categories','categories.id','=','forum_topics.topic_name')
    ->where(['forum_topics.deleted_at'=>NULL])
    ->limit($limit)
    ->offset($offset)
    ->get();

    if (count($topic_list) > 0) {
      return $topic_list;
    }
  }

  public function createTopic(Request $request)
  {
    $requestData = $request->all();
    $this->validate($request,[
      'topic_name'   => 'required',
      'topic_description'   => 'required',
      'user_id'         => 'required'
    ]);
    
    $categoryName = DB::table('categories')->select('category_name')->where('id',$request->topic_name)->first()->category_name;
    $slug_name = $this->slugName->createSlug($categoryName,'forum_topics','topic_slug');
    $requestData['topic_slug']=$slug_name;

    $insert_topic = ForumTopics::create($requestData);

    if ($insert_topic) {
      $data['last_id'] = $insert_topic->id;
    }
    return $data;
  }

  public function viewTopic($id)
  {
    $forum = ForumTopics::select('forum_topics.id', 'topic_name', 'topic_description', 'topic_image', 'user_id','users.first_name','users.last_name')
    ->join('users','forum_topics.user_id','=','users.id')
    ->where(['forum_topics.deleted_at'=>NULL,'forum_topics.id'=>$id])
    ->get();
    if ($forum) {
      return $forum;
    }
  }

  public function updateTopic(Request $request,$id)
  {
    $requestData = $request->all();
    $this->validate($request,[
      'topic_name'   => 'required',
      'topic_description'   => 'required',
      'user_id'         => 'required'
    ]);

    $topic = ForumTopics::findOrFail($id);
    $topic_update = $topic->update($requestData);
    if ($topic_update) {
      return new JsonResponse([],200);
    }
  }

  public function deleteTopic($id)
  {
    $delete_data = ForumTopics::where('id',$id)->delete();
    if ($delete_data) {
      return new JsonResponse([],200);
    }
  }

    /**
   * @api {GET} {base_url}/api/community/question?limit={limit}&offset={offset}&answered=0&likes=0 Questions
   * @apiName Community Questions
   * @apiGroup Community
   * @apiDescription To get community questions
   * @apiHeader {String} Authorization api token key.
  
     * @apiParam {number} limit set to limit the filter results 
     * @apiParam {number} offset set to offset the filter results to a particular record count
     * @apiParam {number} answered  answered passed as 0 for Recently added question and likes,for Popular : answered = 0 and for likes = 1 , for Answered question :  answered   = 1 and likes = 0
     * @apiParam {number} likes  likes passed as 0 for Recently added question
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
    {
   "status_code":"2000",
   "message":"success",
   "body":[
      {
         "question_id":225,
         "question":"test",
         "topic_name":"Incubation Support",
         "publish_date":"2020-01-29 16:39:13",
         "likes":0,
         "dislikes":0,
         "count_comments":0,
         "description":"test",
         "slug":"test-3",
         "first_name":"Wep",
         "last_name":"Niti Aayog",
         "user_id":"wep-niti-aayog",
         "user_type":"super-admin",
         "display_name":"Niti Aayog",
         "profile_pic":"",
         "entreprise_name":""
      },
      {
         "question_id":210,
         "question":"qeqwee1233",
         "topic_name":"Business Validation",
         "publish_date":"2020-01-01 16:14:38",
         "likes":1,
         "dislikes":0,
         "count_comments":0,
         "description":"ewfee",
         "slug":"qeqwee1233",
         "first_name":"Wep",
         "last_name":"Niti Aayog",
         "user_id":"wep-niti-aayog",
         "user_type":"super-admin",
         "display_name":"Niti Aayog",
         "profile_pic":"",
         "entreprise_name":""
      }
   ]
}

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */

  /**
   * This function provide the list of all taxes.
   *
   * @param mixed[] $request Request structure to get the post data for pagination like limit and offset.
   *
   * @var int $limit Should contain a number for limit of record by default it is 10.
   * @var int $offset Should contain a number for offset of record by default it is 0.
   *
   * @return array of objects.
   *
   */ 
  public function getQuestionsListing(Request $request)
  {
    if($request->has('limit')) {
      $limit = $request->get('limit');
    }else{
      $limit = 10;
    }
    if($request->has('offset')) {
      $offset = $request->get('offset');
    }else{
      $offset = 0;
    }

    if ($request->has('likes') && $request->get('likes') == 1) {
      $question_list = ForumQuestions::select(DB::raw('DISTINCT (forum_questions.forum_question)','forum_questions.id'), 'forum_questions.id','forum_questions.forum_question','categories.category_name as topic_name', 'forum.forum_name',  'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.forum_question_slug','forum_questions.description','users.first_name','users.last_name','users.user_slug as user_id', 'users.user_type','user_profile.profile_pic','users.display_name','user_profile.gender','user_forum_like_mapper.like_status')
      ->leftjoin('users','users.id','=','forum_questions.user_id')
      ->leftjoin('user_profile','users.id','=','user_profile.user_id')
      ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
      ->leftjoin('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
      ->leftjoin('categories','categories.id','=','forum_topics.topic_name')
      ->join('user_forum_like_mapper','user_forum_like_mapper.question_id','=','forum_questions.id')
      ->where('user_forum_like_mapper.like_status', '=','1')
      //->where('user_forum_like_mapper.user_id', '=',$this->user_id);
      ->where(['forum_questions.deleted_at'=>NULL])
      ->where(['forum_questions.status'=>'Approved'])
      ->orderBy('forum_questions.updated_at','desc')
      ->limit($limit)
      ->offset($offset)
      ->get();
    } else if ($request->has('answered') && $request->get('answered') == 1) {
      $user_id = $this->user_id;
     // try {

        // $question_list = ForumQuestions::select(DB::raw('DISTINCT(forum_questions.id)','forum_questions.forum_question','forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','forum_topics.topic_name','users.first_name','users.last_name,users.user_slug as user_id','users.user_type','user_profile.profile_pic','user_profile.gender','user_forum_like_mapper.like_status'))
        // ->join('users','users.id','=','forum_questions.user_id')
        // ->leftjoin('user_profile','users.id','=','user_profile.user_id')
        // ->leftjoin('forum_thread','forum_thread.question_id','=','forum_questions.id')
        // ->leftjoin('user_forum_like_mapper', function($join) {
        //   $join->on('user_forum_like_mapper.question_id','=','forum_questions.id');
        //   $join->where('user_forum_like_mapper.user_id', '=',$this->user_id);
        // })
        // ->leftjoin('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
        // ->where(['forum_questions.deleted_at'=>NULL])
        // ->where(['forum_questions.status'=>'Approved'])
        // ->orderBy('forum_questions.updated_at','desc')
        // ->limit($limit)
        // ->offset($offset)
        // ->get();

     // try {
        $question_list = DB::select('SELECT distinct(t1.id),t1.forum_question,t1.user_id,t1.likes,t1.dislikes,t1.created_at as publish_date,t1.status,t1.description,t1.forum_question_slug,t3.topic_name,users.first_name,users.last_name,users.user_slug as user_id,users.user_type,user_profile.profile_pic,user_forum_like_mapper.like_status
          FROM forum_questions as t1
          JOIN users on t1.user_id = users.id
         left  JOIN user_profile on user_profile.user_id = users.id
         JOIN forum_thread as t2 on t2.question_id=t1.id
            JOIN forum_topics as t3 on t3.topic_name=t1.topic_id
          left join user_forum_like_mapper on  user_forum_like_mapper.question_id =t1.id 
         
          where t1.status="Approved" and t2.deleted_at is null and t1.deleted_at is null
          
          ORDER BY t2.updated_at DESC
          LIMIT ' . (int) $offset . ', ' . (int) $limit);
      //} catch (Exception $e) {
       // return ['message' => 'something went wrong'];
      //}
    } else if ($request->has('category') && $request->get('category') > 0) {
      $form_topic_id = ForumTopics::where('topic_name',$request->get('category'))->pluck('id');
      $question_list = ForumQuestions::select('forum_questions.id','categories.category_name as topic_name', 'forum.forum_name', 'forum_questions.forum_question', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','users.first_name','users.last_name','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','users.display_name','user_profile.gender','user_forum_like_mapper.like_status')
      ->join('users','users.id','=','forum_questions.user_id')
      ->leftjoin('user_profile','users.id','=','user_profile.user_id')
      ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
      ->leftjoin('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
      ->leftJoin('user_forum_like_mapper', function($join) {
        $join->on('user_forum_like_mapper.question_id','=','forum_questions.id');
        $join->where('user_forum_like_mapper.user_id', '=',$this->user_id);
      })

      ->join('categories','categories.id','=','forum_topics.topic_name')
      ->where(['forum_questions.deleted_at'=>NULL])
      ->where(['forum_questions.status'=>'Approved'])
      ->whereIn('forum_questions.topic_id',$form_topic_id)
      ->orderBy('forum_questions.created_at','desc')
      ->limit($limit)
      ->offset($offset)
      ->get();
    }
    else {
      $question_list = DB::table('forum_questions')->select(DB::raw('DISTINCT (forum_questions.forum_question)','forum_questions.id'), 'forum_questions.id','forum_questions.forum_question', 'categories.category_name as topic_name', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','users.display_name','user_profile.gender','user_forum_like_mapper.like_status')
      ->join('users','users.id','=','forum_questions.user_id')
      ->leftjoin('user_profile','users.id','=','user_profile.user_id')
      ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
      ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
      ->leftjoin('categories','categories.id','=','forum_topics.topic_name')
      ->leftJoin('user_forum_like_mapper', function($join) {
        $join->on('user_forum_like_mapper.question_id','=','forum_questions.id');
        $join->where('user_forum_like_mapper.user_id', '=',$this->user_id);
      })
      ->where(['forum_questions.deleted_at'=>NULL])
      ->where(['forum_questions.status'=>'Approved'])
      ->limit($limit)
      ->offset($offset)
      ->groupBy('forum_questions.id')
      ->orderBy('forum_questions.created_at','desc')
      ->get();


    }
    $questions = array();
    if (isset($question_list) && count($question_list) > 0) {
      foreach ($question_list as $key1 => $value1) {
        $questions[$key1]['question_id'] = $value1->id;
        $questions[$key1]['question'] = $value1->forum_question;
        $questions[$key1]['topic_name'] = $value1->topic_name;
        $questions[$key1]['publish_date'] = $value1->publish_date;
        $questions[$key1]['likes'] = QuestionLikeMapper::where([
          'question_id' => $value1->id,
          'like_status' => 1,
        ])->count();
        $questions[$key1]['dislikes'] = QuestionLikeMapper::where([
          'question_id' => $value1->id,
          'like_status' => 0,
        ])->count();

        $questions[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$questions[$key1]['question_id'])->count();
       // $questions[$key1]['description'] = substr($value1->description,0,200);
        $questions[$key1]['description'] = $value1->description;
        $questions[$key1]['slug'] = $value1->forum_question_slug;
        $questions[$key1]['first_name'] = isset($value1->first_name)?$value1->first_name:"";
        $questions[$key1]['last_name'] = isset($value1->last_name)?$value1->last_name:"";
        $questions[$key1]['gender'] = isset($value1->gender)?$value1->gender:"";
        $questions[$key1]['user_id'] = isset($value1->user_id)?$value1->user_id:"";
        $questions[$key1]['user_type'] = isset($value1->user_type)?$value1->user_type:"";
        $questions[$key1]['display_name'] = isset($value1->display_name)?$value1->display_name:"";
        $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:"";
        $questions[$key1]['entreprise_name'] = isset($value1->entreprise_name)?$value1->entreprise_name:"";
        $questions[$key1]['like_status'] = isset($value1->like_status)?$value1->like_status:"";
        if($value1->user_id && $value1->user_type && $value1->user_type == 'partner') {
          $slug = DB::table('partner_profile')->where('user_id',$value1->user_id)->first();
          $questions[$key1]['partner_slug'] = $slug ? $slug->slug:'';
          $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:(DB::table('partner_profile')->where('slug', $value1->user_id)->first()?DB::table('partner_profile')->where('slug', $value1->user_id)->first()->logo:'');
        }
      }

      if (count($questions) > 0) {
        return $questions;
      }
    } else {
      return $questions;
    }
  }

  public function createQuestions(Request $request)
  {
    $requestData = $request->all();
    $this->validate($request,[
      'topic_id'   => 'required',
      'forum_question'   => 'required'
    ]);
    $requestData['user_id'] = $this->user_id;
    $insert_question = ForumQuestions::create($requestData);

    if ($insert_question) {
      $data['last_id'] = $insert_question->id;
    }
    return $data;
  }

   /**
   * @api {GET} {base_url}/api/community/question/{slug} View Question
   * @apiName View Community Question
   * @apiGroup Community
   * @apiDescription To view community questions
   * @apiHeader {String} Authorization api token key.
   * @apiParam {string} slug  slug slug of question
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
    {
   "status_code":"2000",
   "message":"success",
   "body":{
      "question":[
         {
            "id":211,
            "topic_id":14,
            "forum_question":"Duplicate check for registration know how",
            "forum_question_slug":"duplicate-check-for-registration-know-how",
            "description":"erreg",
            "likes":2,
            "dislikes":1,
            "posted_date":"0000-00-00",
            "publish_date":"2020-01-01 16:15:57",
            "status":"Approved",
            "topic_name":14,
            "topic_slug":"registration-know-how",
            "forum_name":null,
            "first_name":"Wep",
            "last_name":"Niti Aayog",
            "profile_pic":null,
            "user_id":"wep-niti-aayog",
            "entreprise_name":null,
            "like_status":1,
            "user_type":"super-admin",
            "display_name":"Niti Aayog"
         }
      ],
      "suggested_list":[
         {
            "id":140,
            "topic_id":14,
            "forum_question":"reg know-how",
            "forum_question_slug":"reg-know-how",
            "description":"reg know-how\nreg know-how\nreg know-how\nreg know-how",
            "likes":0,
            "dislikes":0,
            "posted_date":"0000-00-00",
            "publish_date":"2019-05-13 17:06:03",
            "status":"Approved",
            "topic_name":null,
            "forum_name":null,
            "first_name":"Tester",
            "last_name":"p",
            "profile_pic":"1546603262422fc48b309a8e76c8c00cd1b0853601.jpg",
            "user_id":"tester-p",
            "entreprise_name":null,
            "user_type":"entrepreneur"
         }
      ],
      "questions":[
         {
            "thread_count":0
         }
      ]
   }
}

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */

  /**
   * This function provide the list of all taxes.
   *
   * @param mixed[] $request Request structure to get the post data for pagination like limit and offset.
   *
   * @var int $limit Should contain a number for limit of record by default it is 10.
   * @var int $offset Should contain a number for offset of record by default it is 0.
   *
   * @return array of objects.
   *
   */ 
  public function viewQuestions($slug)
  {
    $forumQuestion = ForumQuestions::where('forum_question_slug',$slug)->first();
    if(!$forumQuestion) {
      return new JsonResponse(['message'=> 'No data found'],200);
    }
    $id = $forumQuestion->id;
    $user_id = $this->user_id;
    $data['question'] = ForumQuestions::select('forum_questions.id','forum_questions.topic_id','forum_questions.forum_question','forum_questions.forum_question_slug', 'forum_questions.description','forum_questions.likes','forum_questions.dislikes','forum_questions.posted_date','forum_questions.created_at as publish_date','forum_questions.status','forum_topics.topic_name','forum_topics.topic_slug', 'forum.forum_name','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name','user_forum_like_mapper.like_status', 'users.user_type','users.display_name','user_profile.gender')
    ->leftjoin('users','forum_questions.user_id','=','users.id')
    ->leftjoin('user_profile','user_profile.user_id','=','users.id')
    ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
    ->leftjoin('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
    ->leftjoin('user_forum_like_mapper', function($join) use($user_id){
     $join->on('forum_questions.id', '=', 'user_forum_like_mapper.question_id');
     $join->where('user_forum_like_mapper.user_id', '=', $user_id);
   })
    ->where(['forum_questions.deleted_at'=>NULL])
    ->where('forum_questions.id','=',$id)
    ->get(); 
    
    if($data['question'][0]->user_type == 'partner') {
      $data['question'][0]->partner_slug = DB::table('partner_profile')->where('user_id',$data['question'][0]->user_id)->first()?DB::table('partner_profile')->where('user_id',$data['question'][0]->user_id)->first()->slug:'';
      $data['question'][0]->profile_pic = isset($data['question'][0]->profile_pic)?$data['question'][0]->profile_pic:(DB::table('partner_profile')->where('slug', $data['question'][0]->user_id)->first()?DB::table('partner_profile')->where('slug', $data['question'][0]->user_id)->first()->logo:'');
    }
    $topic_id =(isset($data['question'][0]->topic_id)?$data['question'][0]->topic_id:NULL);
    $question_id = (isset($data['question'][0]->id)?$data['question'][0]->id:NULL);

    if(isset($data['question'][0]) && !empty($data['question'][0])){

      $data['question'][0]->likes = QuestionLikeMapper::where([
        'question_id' => $id,
        'like_status' => 1,
      ])->count();

      $data['question'][0]->dislikes = QuestionLikeMapper::where([
        'question_id' => $id,
        'like_status' => 0,
      ])->count();

      $data['question'][0]->likes = QuestionLikeMapper::where([
        'question_id' => $id,
        'like_status' => 1,
      ])->count();
    }

    $data['suggested_list'] = ForumQuestions::select('forum_questions.id','forum_questions.topic_id','forum_questions.forum_question','forum_questions.forum_question_slug', 'forum_questions.description','forum_questions.likes','forum_questions.dislikes','forum_questions.posted_date','forum_questions.created_at as publish_date','forum_questions.status','forum_topics.topic_name', 'forum.forum_name','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','user_profile.gender')
    ->join('users','forum_questions.user_id','=','users.id')
    ->join('user_profile','user_profile.user_id','=','users.id')
    ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
    ->leftjoin('forum_topics','forum_topics.id','=','forum_questions.topic_id')
    ->leftjoin('user_forum_like_mapper','forum_questions.id','=','user_forum_like_mapper.question_id')
    ->where(['forum_questions.deleted_at'=>NULL])
    ->where('forum_questions.topic_id','=',$topic_id)
    ->where('forum_questions.id', '!=' , $question_id)
    ->groupBy('forum_questions.id')
    ->get();

    if ($data['question']) {
      $questions = (isset($data['question'][0])?$data['question'][0]:NULL);
      $data['questions'][0]['thread_count']=ForumThread::select('forum_thread.id')->where(['question_id'=>$id,'parent_id'=>NULL])->count();
      return $data;
    }
  }

/**
     * @api {PATCH} {base_url}/api/community/question/{slug} Update
     * @apiName Update my forum question
     * @apiGroup My Forum
     * @apiParam {string} description  update description
     * @apiParam {string} forum_question  update forum question
     * @apiParam {number} topic_id  id for updating question
     * @apiParam {string} slug  question slug
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
          "status_code":"2000",
          "message":"success",
          "body":[

          ]
        }

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
     public function updateQuestions(Request $request,$slug)
     {
      $requestData = $request->all();
      $this->validate($request,[
        'topic_id'   => 'required',
        'forum_question'   => 'required'
      ]);

      $requestData['user_id'] =  $this->user_id;
      
      $question = ForumQuestions::where('forum_question_slug',$slug)->first();
      
      $threadCount = ForumThread::where('question_id',$question->id)->count();
      
      if($threadCount > 0) {
        return new JsonResponse(['message'=>"Question cannot be edited"],400);
      }
      $question_update = $question->update($requestData);

      if ($question_update) {
        return new JsonResponse([],200);
      }
    }

     /**
   * @api {patch} {base_url}/api/community/delete-question/{slug} Delete Question
   * @apiName  Delete Community question
   * @apiGroup Dashboard Community
   * @apiDescription To delete community questions category wise.
   * @apiHeader {String} Authorization api token key.
     * @apiParam {string} slug  pass forum_topic as slug to be deleted
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
    {
      "status_code":"2000",
      "message":"success",
      "body":[

      ]
    }

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */

  /**
   * This function provide the list of all taxes.
   *
   * @param mixed[] $request Request structure to get the post data for pagination like limit and offset.
   *
   * @var int $limit Should contain a number for limit of record by default it is 10.
   * @var int $offset Should contain a number for offset of record by default it is 0.
   *
   * @return array of objects.
   *
   */ 
  public function deleteQuestions(Request $request, $slug)
  {
    $user = Auth::user();
    $data = ForumQuestions::with('topic')->where('forum_question_slug',$slug)->first();
    if($data && $request->forum_topic==$data->topic->topic_slug) {
      $topic = ForumTopics::where('topic_slug', $request->forum_topic)->first();
      if($user->id != $data->user_id && !$user->can('deactivate',$topic)) {
        return new JsonResponse([],403);
      }
      ForumThread::where('question_id',$data->id)->delete();
      QuestionLikeMapper::where('question_id',$data->id)->delete();
      $data->delete();
      return new JsonResponse([],200);
    }
    
    return new JsonResponse(['message'=>'failed to delete'],200);
    
  }

  public function getThreadListing(Request $request)
  {
    if($request->has('limit')) {
      $limit = $request->get('limit');
    }else{
      $limit = 10;
    }
    if($request->has('offset')) {
      $offset = $request->get('offset');
    }else{
      $offset = 0;
    }

    $thread_list = ForumThread::select('forum_thread.id', 'forum_thread.thread_answer','forum_thread.slug','forum_thread.likes','forum_questions.forum_question', 'forum_questions.user_id', 'forum_thread.parent_id','users.first_name','users.last_name','user_profile.profile_pic','user_profile.entreprise_name', 'users.user_type','users.display_name')
    ->join('users','users.id','=','forum_thread.user_id')
    ->join('user_profile','users.id','=','user_profile.user_id')
    ->leftjoin('forum_questions','forum_questions.id','=','forum_thread.question_id')
    ->where(['forum_questions.deleted_at'=>NULL])
    ->limit($limit)
    ->offset($offset)
    ->get();

    if (count($thread_list) > 0) {
      $data['count'] = count($thread_list);
      $data['body'] = $thread_list;
      return $data;
    }
  }

    /**
   * @api {post} {base_url}/api/community/thread Add Answer / Comment
   * @apiName  Add Answer to Community Questions
   * @apiGroup Community
   * @apiDescription To store answer in community questions
   * @apiHeader {String} Authorization api token key.
     * @apiParam {number} forum_question_slug  forum_question_slug pass question slug to add answer for particular question
     * @apiParam {number} thread_answer  thread_answer add answer
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
      {
      "status_code":"2000",
      "message":"success",
      "body":{
          "last_id":999
      }
    }

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */

  /**
   * This function provide the list of all taxes.
   *
   * @param mixed[] $request Request structure to get the post data for pagination like limit and offset.
   *
   * @var int $limit Should contain a number for limit of record by default it is 10.
   * @var int $offset Should contain a number for offset of record by default it is 0.
   *
   * @return array of objects.
   *
   */
  public function createThread(Request $request)
  {
    try {
      $requestData = $request->all();
      $this->validate($request,[
        'forum_question_slug'   => 'required',
        'thread_answer'   => 'required',
        'user_id'         => 'required'
      ]);
      $question_id = ForumQuestions::where('forum_question_slug',$request->forum_question_slug)->first()->id;
      if($question_id) {
        $requestData['question_id'] = $question_id;
        $insert_thread = ForumThread::create($requestData);

        if ($insert_thread) {
          $data['last_id'] = $insert_thread->id;
        }
        return $data;
      } else {
        $data['status_code'] = '404';
        $data['message'] = "No Question found";
        return $data;
      }
    } catch(Exception $e) {
      $data['status'] = 500;
      $data['message'] = $e->getMessage();
      return $data;
    }
  }

     /**
   * @api {GET} {base_url}/api/community/thread/{slug}?limit={limit}&offset={limit} Thread
   * @apiName  Question Thread
   * @apiGroup Community
   * @apiDescription To view community questions  threads
   * @apiHeader {String} Authorization api token key.
   * @apiParam {string} slug  slug slug of question
   * @apiParam {number} limit set to limit the filter results 
  * @apiParam {number} offset set to offset the filter results to a particular record count
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
   {
   "status_code":"2000",
   "message":"success",
   "body":{
      "count":1,
      "thread":[
         {
            "id":997,
            "question_id":211,
            "forum_question":"Duplicate check for registration know how",
            "likes":0,
            "user_slug":"wep-niti-aayog",
            "user_type":"super-admin",
            "first_name":"Wep",
            "parent_id":null,
            "last_name":"Niti Aayog",
            "publish_date":{
               "date":"2020-01-29 19:26:46.000000",
               "timezone_type":3,
               "timezone":"Asia\/Kolkata"
            },
            "display_name":"Niti Aayog",
            "like_status":0,
            "profile_pic":null,
            "entreprise_name":null,
            "thread_answer":"demo\n",
            "thread_count":0
         }
      ]
   }
}

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */

  /**
   * This function provide the list of all taxes.
   *
   * @param mixed[] $request Request structure to get the post data for pagination like limit and offset.
   *
   * @var int $limit Should contain a number for limit of record by default it is 10.
   * @var int $offset Should contain a number for offset of record by default it is 0.
   *
   * @return array of objects.
   *
   */
  public function viewThread(Request $request,$slug)
  {
    if($request->has('limit')) {
      $limit = $request->get('limit');
    }else{
      $limit = 10;
    }
    if($request->has('offset')) {
      $offset = $request->get('offset');
    }else{
      $offset = 0;
    }
    $user_id = $request->user_id;
    $query = ForumQuestions::where('forum_question_slug',$slug)->first();
    if(!$query)  {
      return new JsonResponse(["message"=>"No Data found"],200);
    }
    $id = $query->id;
    
    $thread = DB::table('forum_thread as ft')->select(DB::raw('DISTINCT(ft.id) as thread_id'),'ft.question_id as question_id','ft.thread_answer','ft.parent_id','ft.user_id as user_id','ft.likes','ft.created_at', 'forum_questions.forum_question','users.first_name','users.last_name','user_profile.profile_pic','forum_questions.created_at as publish_date ','user_profile.entreprise_name', 'users.user_type','users.user_slug','users.display_name','user_profile.gender')
    ->leftjoin('forum_questions','forum_questions.id','=','ft.question_id')
    ->leftjoin('users','users.id','=','ft.user_id')
    ->leftjoin('user_profile','users.id','=','user_profile.user_id')
    ->where(['ft.deleted_at'=>NULL])
    ->where('forum_questions.id','=',$id);
    if(isset($request->mycomment) && $request->mycomment == 'true') {
      $thread = $thread->leftjoin('forum_thread as ft2','ft.id','=','ft2.parent_id')
      ->where('ft.user_id',$user_id);
    } else if (isset($request->mycomment) && $request->mycomment == 'false') {
     $thread = $thread->where(['ft.parent_id'=>NULL]);
   }
   $thread = $thread->limit($limit)
   ->orderBy('ft.id', 'desc')
   ->offset($offset)
   ->get();
   
    //$thread_count = $thread->count();

    //dd($thread_count);

   $thread_list = array();
   foreach ($thread as $key => $value) {
    $thread_list[$key]['id'] = $value->thread_id;
    $thread_list[$key]['question_id'] = $value->question_id;
    $thread_list[$key]['forum_question'] = $value->forum_question;
    $thread_list[$key]['likes'] = $value->likes;
    $thread_list[$key]['user_slug'] = $value->user_slug;
    $thread_list[$key]['user_type'] = $value->user_type;
    $thread_list[$key]['first_name'] = $value->first_name ;
    $thread_list[$key]['gender'] = $value->gender ;
    $thread_list[$key]['parent_id'] = $value->parent_id;
    $thread_list[$key]['last_name'] =$value->last_name;
    $thread_list[$key]['publish_date'] =$value->created_at;
    $thread_list[$key]['display_name'] =$value->display_name?$value->display_name:'';
    $status = QuestionLikeMapper::where('thread_id',$value->thread_id)->where('user_id',$this->user_id)->first();
    if($status) {
      $thread_list[$key]['like_status']= $status->like_status;
    }else {
      $thread_list[$key]['like_status']=0;
    }
    if($value->user_type == 'partner') {
      $thread_list[$key]['profile_pic'] =  isset($value->profile_pic)?$value->profile_pic:(DB::table('partner_profile')->where('user_id', $value->user_id)->first()?DB::table('partner_profile')->where('user_id', $value->user_id)->first()->logo:'');
    }else {
      $thread_list[$key]['profile_pic'] = $value->profile_pic;
    }
    $thread_list[$key]['entreprise_name'] = $value->entreprise_name;
    if (isset($value->parent_id) && $value->parent_id != NULL) {
      $thread_list[$key]['thread_answer']['child_thread'] = $value->thread_answer;
    }else {
      $thread_list[$key]['thread_answer'] = $value->thread_answer;
    }
     if($value->user_type == 'partner') {
        $thread_list[$key]['partner_slug'] = DB::table('partner_profile')->where('user_id',$value->user_id)->first();
      }
      $thread_list[$key]['thread_count']=ForumThread::where(['question_id'=>$id,'parent_id'=>$value->thread_id])->count();
    
  }
  if (count($thread_list) > 0) {
    $data['count'] = count($thread);
      //ForumThread::select('forum_thread.id')->where('question_id',$id)->whereNull('forum_thread.parent_id')->count();
    $data['thread'] = $thread_list;
    return $data;
  }
}


public function updateThread(Request $request,$id)
{
  $requestData = $request->all();
  $this->validate($request,[
    'question_id'   => 'required',
    'thread_answer'   => 'required'
  ]);
  $requestData['user_id'] = $this->user_id;
  $thread = ForumThread::findOrFail($id);
  if($thread->likes == 0) {
    $thread_update = $thread->update($requestData);
    if ($thread_update) {
      return new JsonResponse([],200);
    }
  } else {
    return new JsonResponse(['message'=> "User cannot edit"],200);
  }
}

public function deleteThread(Request $request, $id)
{
  $user = Auth::user();
  $thread = ForumThread::with('question.topic')->find($id);
  if($thread && $thread->question->topic->topic_slug == $request->forum_topic) {
    $topic = ForumTopics::where('topic_slug', $request->forum_topic)->first();
    if($user->id != $thread->user_id && !$user->can('deactivate',$topic)) {
      return new JsonResponse([],403);
    }
  }
  ForumThread::where('parent_id',$id)->delete();
  $delete_data = $thread->delete();
  if ($delete_data) {
    return new JsonResponse([],200);
  }
}

public function addLikesQuestion(Request $request)
{

  $question = ForumQuestions::findOrFail($request->id);
  $likes_count = ($question->likes)+($request->likes);
  $update_likes = ForumQuestions::where('id',$request->id)->update([
    'likes' => $likes_count
  ]);

  if (isset($update_likes)) {
    $data['likes'] = ($question->likes)+($request->likes);
  }
  return $data;

}


   /**
   * @api {Post} {base_url}/api/community/thread/like Like Answer
   * @apiName  Add Like to Answer in Community Questions
   * @apiGroup Community
   * @apiDescription To add like answer in community questions
   * @apiHeader {String} Authorization api token key.
     * @apiParam {number} like_status  like_status it is set to 1 to increase the like count
     * @apiParam {number} thread_id  thread_id answer to be liked for particular questions id.
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
      {
        "status_code":"2000",
        "message":"success",
        "body":{
            "insert_value":105,
            "updated_likes_counter":1,
            "like_status":"1"
        }
      }

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */

  /**
   * This function provide the list of all taxes.
   *
   * @param mixed[] $request Request structure to get the post data for pagination like limit and offset.
   *
   * @var int $limit Should contain a number for limit of record by default it is 10.
   * @var int $offset Should contain a number for offset of record by default it is 0.
   *
   * @return array of objects.
   *
   */
  public function addLikesThread(Request $request)
  {
    $requestData = $request->all();
    $data = QuestionLikeMapper::select('user_id','thread_id','like_status')->where(['user_id'=>$this->user_id,'thread_id'=>$request->thread_id,'deleted_at'=>NULL])
    ->get();
    $thread = ForumThread::findOrFail($request->thread_id);
    if(count($data)==0){
      $data['insert_value'] = QuestionLikeMapper::create($requestData)->id;
      $likes_count = QuestionLikeMapper::where(['thread_id'=>$request->thread_id,'like_status'=>'1','deleted_at'=>NULL])
      ->count();
    } else {
      $data['insert_value'] =  QuestionLikeMapper::where(['user_id'=>$this->user_id,'thread_id'=>$request->thread_id])
      ->update(['like_status' => $request->like_status]);
      $likes_count = QuestionLikeMapper::where(['thread_id'=>$request->thread_id,'like_status'=>'1','deleted_at'=>NULL])
      ->count();
    }
    $update_likes = ForumThread::where('id',$request->thread_id)->update([
      'likes' => $likes_count
    ]);
    if (isset($update_likes)) {
      $data['updated_likes_counter'] = $likes_count;
      $data['like_status'] = $request->like_status;
    }
    return $data;
  }

     /**
   * @api {GET} {base_url}/api/community/top-contributor Top Contributor
   * @apiName Top Contributor in Community
   * @apiGroup Community
   * @apiDescription To get community questions
   * @apiHeader {String} Authorization api token key.
     * @apiParam {number} limit set to limit the filter results 
     * @apiParam {number} offset set to offset the filter results to a particular record count
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
      {
      "status_code":"2000",
      "message":"success",
      "body":[
          {
            "first_name":"GEN India",
            "last_name":"",
            "user_slug":"gen-india",
            "count":51,
            "profile_pic":"partner-gen-india.png",
            "entreprise_name":null,
            "user_type":"partner"
          },
          {
            "first_name":"Sanity",
            "last_name":"Test",
            "user_slug":"sanity-test",
            "count":27,
            "profile_pic":"1549613237a94c684474f600c1d46b24fd8d16b2a4.png",
            "entreprise_name":null,
            "user_type":"entrepreneur"
          }
      ]
    }

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */

  /**
   * This function provide the list of all taxes.
   *
   * @param mixed[] $request Request structure to get the post data for pagination like limit and offset.
   *
   * @var int $limit Should contain a number for limit of record by default it is 10.
   * @var int $offset Should contain a number for offset of record by default it is 0.
   *
   * @return array of objects.
   *
   */
  public function topContributor(){
    $topContributor = ForumThread::select('users.first_name','users.last_name','users.user_slug as user_slug',DB::raw('COUNT(forum_thread.user_id) as count'),'user_profile.profile_pic','user_profile.entreprise_name', 'users.user_type','user_profile.gender')
    ->join('users','users.id','=','forum_thread.user_id')
    ->leftjoin('user_profile','forum_thread.user_id','=','user_profile.user_id')
    ->whereNotIn('users.user_type',  ['admin','super-admin','team-member'])
    ->where(['users.deleted_at' =>null,'user_profile.deleted_at'=>null,'forum_thread.deleted_at'=>null,'user_profile.deleted_at'=>null,'users.deleted_at'=>null])
    ->groupBy('forum_thread.user_id')
    ->orderBy('count', 'desc')
    ->take(5)
    ->get();
    
    if(count($topContributor)>0) {
      foreach($topContributor as $value) {
        if($value->user_type == 'partner') {

          if($value->profile_pic == NULL){
              // echo "slug is ".$value->user_slug; exit;
            $profile_pic=  DB::table('partner_profile')->select('logo')->where('slug',$value->user_slug)->first();
            if($profile_pic)
            {
              $value->profile_pic = $profile_pic->logo; 
            }else{
              $value->profile_pic = NULL;
            }
          }
          
            // print_r($value->profile_pic); exit;
            // echo $value->profile_pic."<br>";
        }
      }
    }
    return $topContributor;
  }

  public function search(Request $request,$search_topic = 'all', $search_keyword = null ) {
    $search_keyword = $request['search_keyword'] = urldecode($search_keyword);
    $category = $request['search_category'] = urldecode($search_topic);
    $request['limit'] = ($request['limit']) ? $request['limit'] : 10;
    $request['offset'] = ($request['offset']) ? $request['offset'] : 0;
    $this->validate($request, [
      'search_keyword' => 'required|string',
      'limit' => 'nullable|numeric|min:10|max:100',
      'offset' => 'nullable|numeric|min:0|max:10000'
    ]);
    $search_results = [];
    if ($request['search_category'] == 'all') {
      try {
        $results = DB::table('forum_questions')
        ->where('forum_question', 'LIKE', "%{$search_keyword}%")
        ->where(['forum_questions.deleted_at'=>NULL])
        ->skip($request['offset'])
        ->take($request['limit'])
        ->get(['id', 'forum_question as search_title', 'forum_question_slug as search_slug', 'description as search_excerpt']);
      } catch (Exception $exc) {
        return array($exc->getMessage());
      }
    } else {
      try {
        $results = DB::table('forum_questions')
        ->whereRaw('topic_id IN (SELECT id from forum_topics WHERE topic_slug = ?)', $category)
        ->where('forum_question', 'LIKE', "%{$search_keyword}%")
        ->where(['forum_questions.deleted_at'=>NULL])
        ->skip($request['offset'])
        ->take($request['limit'])
        ->get(['id', 'forum_question as search_title', 'forum_question_slug as search_slug', 'description as search_excerpt']);
      } catch (Exception $exc) {
        return array($exc->getMessage());
      }
    }
    $search_results['topic'] = $category;
    $search_results['search_keyword'] = $search_keyword;
    $search_results['results_offset'] = $request->query('offset');
    $search_results['results_limit'] = $request->query('limit');
    $search_results['results_count'] = $results->count();
    $search_results['results_data'] = $results;
    return $search_results;
  }

  // public function updateLikeStatus(Request $request){
  //   try {
  //     $user_id = $request->user_id;
  //     $forum_question_slug = $request->forum_question_slug;
  //     $like_status = $request->like_status;
  //     $requestData = $request->all();
  //     $question_id = ForumQuestions::where('forum_question_slug',$forum_question_slug)->first()->id;
      
  //     if($question_id) {
  //       $requestData['question_id'] = $question_id;
  //       $data = QuestionLikeMapper::select('user_id','question_id')->where(['user_id'=>$request->user_id,'question_id'=>$question_id,'deleted_at'=>NULL])
  //       ->get();

  //       if(count($data)==0){
  //         $data['insert_value'] = QuestionLikeMapper::create($requestData)->id;
  //       }else{
  //         $data['insert_value'] =  QuestionLikeMapper::where(['user_id'=>$request->user_id,'question_id'=>$question_id])
  //         ->update(['like_status' => $request->like_status]);
  //       }
  //       return $data['insert_value'];
  //     } else {
  //       $data['status'] = 404;
  //       $data['message'] = "No Question found";
  //       return $data;
  //     }
  //   } catch(Exception $e) {
  //     $data['status'] = 500;
  //     $data['message'] = $e->getMessage();
  //     return $data;
  //   }
  // }

   public function updateLikeStatus(Request $request){
     try {
      $user_id = $request->user_id;
       $forum_question_slug = $request->forum_question_slug;
      $like_status = $request->like_status;
      $requestData = $request->all();
     // $blog_id = $request->blog_id;
      $question_id = ForumQuestions::where('forum_question_slug',$forum_question_slug)->first()->id;
        $data = QuestionLikeMapper::select('user_id','question_id')->where(['user_id'=>$this->user_id,'question_id'=>$question_id,'deleted_at'=>NULL]) ->get()->transform(function($item,$Key){
            $item->user_slug = Users::where('id',$item->user_id)->first()?Users::where('id',$item->user_id)->first()->user_slug:'';
            return $item;
        });
      
      if($question_id) {
        $requestData['question_id'] = $question_id;
        $data = QuestionLikeMapper::select('user_id','question_id')->where(['user_id'=>$request->user_id,'question_id'=>$question_id,'deleted_at'=>NULL])
        ->get();
         
        if(count($data)==0){
           $data['insert_value'] = QuestionLikeMapper::create([
              'user_id' => $this->user_id,
              'question_id'=> $question_id,
              'like_status' => 1
            ]);
            if($data['insert_value']){
              $data['like_update_status'] = $this->addLikesForForum($this->user_id,$question_id,$like_status);
            }
        }else{
          $data['insert_value'] =  QuestionLikeMapper::where(['user_id'=>$request->user_id,'question_id'=>$question_id])
          ->update(['like_status' => $request->like_status]);
          if($data['insert_value']){
              $data['like_update_status'] = $this->addLikesForForum($this->user_id,$question_id,$like_status);
            }
        }
        return $data['insert_value'];
      } else {
        $data['status'] = 404;
        $data['message'] = "No Question found";
        return $data;
      }
    } catch(Exception $e) {
      $data['status'] = 500;
      $data['message'] = $e->getMessage();
      return $data;
    }
  }

   public function addLikesForForum($user_id,$question_id,$like_status){
    $count = QuestionLikeMapper::where(['question_id'=>$question_id,'like_status'=>1])->count();
    $blog = ForumQuestions::findOrFail($question_id);
    $blog->likes = $count;
    $blog->save();
    return $count;
  }

  public function getTime(){
    $date =  new \DateTime();
    echo $date->format('Y-m-d H:i:s'); 
  }

  public function generateForumSlug() {
    try {
      $forum = ForumQuestions::all();
      foreach($forum as $value) {
        $new_slug= $this->slugName->createSlug($value->forum_question,'forum_questions','forum_question_slug',$value->id);
        ForumQuestions::where('id',$value->id)->update(['forum_question_slug'=>$new_slug]);
      } 
      $data['status_code'] = 200;
      $data['message'] = "Slug updated Successfully.";
      return $data;
    } catch (Exception $e) {
      return $e->getMessage();
    }
  }
  public function threadAnswer(Request $request){
    try {
      $this->validate($request,[
        'forum_question_slug'   => 'required',
        'thread_answer'   => 'required',
        'answer_id'       => 'required'
      ]);

      $requestData = $request->all();
      $question_id = ForumQuestions::where('forum_question_slug',$request->forum_question_slug)->first()->id;
      if($question_id) {
        $requestData['question_id'] = $question_id;
        $answer_thread = ForumThread::create(['question_id' => $question_id, 'thread_answer' => $request->thread_answer,'parent_id'=>$request->answer_id,'user_id'=>$request->user_id]);
        if ($answer_thread) {
          $data['last_id'] = $answer_thread->id;
        }
        return $data;
      } else {
        $data['status_code'] = '404';
        $data['message'] = "No thread available for this answer";
        return $data;
      }
    } catch (Exception $e) {
      return $e->getMessage();
    }
  }

 /**
     * @api {get} {base_url}/api/community/user-question User Questions
     * @apiName Get UserQuestionsListing
     * @apiGroup My Forum
     *  @apiParam {number} limit set to limit the filter results 
     * @apiParam {number} offset set to offset the filter results to a particular record count
     * @apiParam {string} keyword  pass entered search key with query string 
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
   "status_code":"2000",
   "message":"success",
   "body":{
      "count":20,
      "questions":[
         {
            "topic_id":14,
            "question_id":226,
            "question":"test",
            "topic_name":"Registration Know-how",
            "publish_date":"2020-01-30 12:22:28",
            "likes":0,
            "dislikes":0,
            "count_comments":1,
            "description":"test",
            "slug":"test-4",
            "first_name":"Wep",
            "last_name":"Niti Aayog",
            "user_id":"wep-niti-aayog",
            "user_type":"super-admin",
            "profile_pic":"",
            "entreprise_name":""
         },
        
         {
            "topic_id":17,
            "question_id":212,
            "question":"duplicate aerty ",
            "topic_name":"aertry12",
            "publish_date":"2020-01-01 16:19:23",
            "likes":0,
            "dislikes":4,
            "count_comments":1,
            "description":"duplicate aerty ",
            "slug":"duplicate-aerty",
            "first_name":"Wep",
            "last_name":"Niti Aayog",
            "user_id":"wep-niti-aayog",
            "user_type":"super-admin",
            "profile_pic":"",
            "entreprise_name":""
         }
      ]
   }
}

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
     public function getUserQuestionsListing(Request $request)
     {
       if($request->has('limit')) {
         $limit = $request->get('limit');
       }else{
         $limit = 10;
       }
       if($request->has('offset')) {
         $offset = $request->get('offset');
       }else{
         $offset = 0;
       }

       $query = ForumQuestions::select('categories.id as topic_id','forum_questions.id', 'categories.category_name as topic_name', 'forum.forum_name', 'forum_questions.forum_question', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.forum_question_slug','forum_questions.description','users.first_name','users.last_name','users.user_slug as user_id', 'users.user_type','user_profile.profile_pic','user_profile.gender','user_forum_like_mapper.like_status')
       ->join('users','users.id','=','forum_questions.user_id')
       ->leftjoin('user_profile','users.id','=','user_profile.user_id')
       ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
       ->leftjoin('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
       ->leftjoin('user_forum_like_mapper', function($join) {
        $join->on('user_forum_like_mapper.question_id','=','forum_questions.id');
        $join->where('user_forum_like_mapper.user_id', '=',$this->user_id);
      })
       ->join('categories','categories.id','=','forum_topics.topic_name')
       ->where(['forum_questions.deleted_at'=>NULL,'forum_questions.user_id'=>$this->user_id])
       ->where(['forum_questions.status'=>'Approved'])
       ->orderBy('forum_questions.updated_at','desc');
       if($request->sort = 'title') {

         $query =  $query->orderBy('forum_questions.forum_question','DESC');
       } 
       else  if($request->sort == 'status' && $request->order == 1) {

        $query =   $query->orderBy('forum_questions.status','DESC');
      } else {
        $query = $query->orderBy('updated_at','DESC');
      }

      $search = str_replace("%20"," ",$request->keyword);
      if(isset($search) && !empty($search)) {

       $query =   $query->where('forum_questions.forum_question','like','%'.$search.'%');
     }

     $count = $query->count();

     $question_list = $query
     ->limit($limit)
     ->offset($offset)
     ->get();

     $questions = array();
     if (isset($question_list) && count($question_list) > 0) {
       foreach ($question_list as $key1 => $value1) {
         $questions[$key1]['topic_id'] = $value1->topic_id;
         $questions[$key1]['question_id'] = $value1->id;
         $questions[$key1]['question'] = $value1->forum_question;
         $questions[$key1]['topic_name'] = $value1->topic_name;
         $questions[$key1]['publish_date'] = $value1->publish_date;
         $questions[$key1]['likes'] = QuestionLikeMapper::where([
           'question_id' => $value1->id,
           'like_status' => 1,
         ])->count();

         $questions[$key1]['dislikes'] = QuestionLikeMapper::where([
           'question_id' => $value1->id,
           'like_status' => 0,
         ])->count();

         $questions[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$questions[$key1]['question_id'])->count();
         $questions[$key1]['description'] = $value1->description;
         $questions[$key1]['slug'] = $value1->forum_question_slug;
         $questions[$key1]['first_name'] = isset($value1->first_name)?$value1->first_name:"";
         $questions[$key1]['last_name'] = isset($value1->last_name)?$value1->last_name:"";
         $questions[$key1]['user_id'] = isset($value1->user_id)?$value1->user_id:"";
         $questions[$key1]['user_type'] = isset($value1->user_type)?$value1->user_type:"";
         $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:"";
         $questions[$key1]['entreprise_name'] = isset($value1->entreprise_name)?$value1->entreprise_name:"";
         $questions[$key1]['like_status'] = isset($value1->like_status)?$value1->like_status:"";

         if($value1->user_id && $value1->user_type && $value1->user_type == 'partner') {
           $slug = DB::table('partner_profile')->where('user_id',$value1->user_id)->first();
           $questions[$key1]['partner_slug'] = $slug ? $slug->slug:'';
           $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:(DB::table('partner_profile')->where('slug', $value1->user_id)->first()?DB::table('partner_profile')->where('slug', $value1->user_id)->first()->logo:'');
         }
       }

     } else {
       return $questions;
     }

     return new JsonResponse(['count'=> $count,'questions'=>$questions]);
   }

  /**
     * @api {get} /thread-answer get specific question answer and thread
     * @apiName threadAnswerList
     * @apiGroup admin
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
         "status_code": "2000",
        "message": "success",
        "body": {
         "count": 1,
        "questions": [
            {
                "question_id": 26,
                "question": "tax",
                "topic_name": "Technology Validation",
                "publish_date": "2019-03-06 07:34:58",
                "likes": 0,
                "dislikes": 0,
                "count_comments": 0,
                "description": "....",
                "slug": "tax-3",
                "first_name": "WEP ADMIN",
                "last_name": "",
                "user_id": "ashutosh",
                "user_type": "super-admin",
                "profile_pic": "admin-pic.png",
                "entreprise_name": ""
            }
        ],
         }

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
     public function threadAnswerList(Request $request){
      if($request->has('question_slug')){
        $slug= $request->question_slug;
        $id =  ForumQuestions::where('forum_question_slug',$slug)->first()->id;
      }
      if(isset($id)){
        $thread = ForumThread::select('forum_thread.id as thread_id','forum_questions.id as question_id','forum_thread.thread_answer','forum_thread.parent_id','forum_thread.user_id as user_id','forum_thread.likes','forum_thread.created_at', 'forum_questions.forum_question','users.first_name','users.last_name','user_profile.profile_pic','forum_questions.created_at as publish_date ','user_profile.entreprise_name', 'users.user_type','users.user_slug','users.display_name','user_profile.gender')
        ->leftjoin('forum_questions','forum_questions.id','=','forum_thread.question_id')
        ->leftjoin('users','users.id','=','forum_thread.user_id')
        ->leftjoin('user_profile','users.id','=','user_profile.user_id')
        ->where(['forum_thread.deleted_at'=>NULL])
        ->where('forum_questions.id','=',$id)
        ->where('forum_thread.parent_id','=',$request->answer_id)
        ->orderBy('forum_thread.created_at', 'desc')
        ->get();
        $thread_list = array();
        foreach ($thread as $key => $value) {
          $thread_list[$key]['id'] = $value->thread_id;
          $thread_list[$key]['question_id'] = $value->question_id;
          $thread_list[$key]['forum_question'] = $value->forum_question;
          $thread_list[$key]['likes'] = $value->likes;
          $thread_list[$key]['user_slug'] = $value->user_slug;
          $thread_list[$key]['user_type'] = $value->user_type;
          $thread_list[$key]['first_name'] = $value->first_name ;
          $thread_list[$key]['parent_id'] = $value->parent_id;
          $thread_list[$key]['last_name'] =$value->last_name;
          $thread_list[$key]['publish_date'] =$value->created_at;
          $thread_list[$key]['display_name'] =$value->display_name;
          $status = QuestionLikeMapper::where('thread_id',$value->thread_id)->where('user_id',$this->user_id)->first();
          if($status) {
            $thread_list[$key]['like_status']= $status->like_status;
          }else {
            $thread_list[$key]['like_status']=0;
          }
          if($value->user_type == 'partner') {
            $thread_list[$key]['profile_pic'] =  isset($value->profile_pic)?$value->profile_pic:(DB::table('partner_profile')->where('user_id', $value->user_id)->first()?DB::table('partner_profile')->where('user_id', $value->user_id)->first()->logo:'');
          }else {
            $thread_list[$key]['profile_pic'] = $value->profile_pic;
          }

          $thread_list[$key]['entreprise_name'] = $value->entreprise_name;
          if (isset($value->parent_id) && $value->parent_id != NULL) {
            $thread_list[$key]['thread_answer']['child_thread'] = $value->thread_answer;
          }else {
            $thread_list[$key]['thread_answer'] = $value->thread_answer;
          }
          if($value->user_type == 'partner') {
            $thread_list[$key]['partner_slug'] = DB::table('partner_profile')->where('user_id',$value->user_id)->first()->slug;
          }
        }
        if ($thread_list) {
          $data['count'] = ForumThread::select('forum_thread.id')->where(['question_id'=>$id,'parent_id'=>$request->answer_id])->count();
          $data['thread'] = $thread_list;
          return $data;
        }
      }
    }

  /**
     * @api {get} /category-question/{slug}?keyword= get specific question based on category and pass value with query string with keyword
     * @apiName getCategoryWiseQuestionsListing
     * @apiGroup admin
     * @apiParam {slug} category slug
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
         "status_code": "2000",
        "message": "success",
        "body": {
         "count": 1,
        "questions": [
            {
                "question_id": 26,
                "question": "tax",
                "topic_name": "Technology Validation",
                "publish_date": "2019-03-06 07:34:58",
                "likes": 0,
                "dislikes": 0,
                "count_comments": 0,
                "description": "....",
                "slug": "tax-3",
                "first_name": "WEP ADMIN",
                "last_name": "",
                "user_id": "ashutosh",
                "user_type": "super-admin",
                "profile_pic": "admin-pic.png",
                "entreprise_name": ""
            }
        ],
         }
          
         
         
         * @apiSuccessExample Success-Response-2000: Search result
  {
    "status_code": "2000",
    "message": "success",
    "body": {
        "count": 3,
        "questions": [
            {
                "question_id": 159,
                "question": "test q2",
                "topic_name": "Incubation Support",
                "publish_date": "2019-05-14 11:34:45",
                "likes": 0,
                "dislikes": 0,
                "count_comments": 8,
                "description": "test q2",
                "slug": "test-q2",
                "first_name": "rupesh",
                "last_name": "",
                "user_id": "ashutosh",
                "user_type": "super-admin",
                "profile_pic": "Ashutosh.jpg",
                "entreprise_name": ""
            },
            {
                "question_id": 159,
                "question": "test q2",
                "topic_name": "Incubation Support",
                "publish_date": "2019-05-14 11:34:45",
                "likes": 0,
                "dislikes": 0,
                "count_comments": 8,
                "description": "test q2",
                "slug": "test-q2",
                "first_name": "rupesh",
                "last_name": "",
                "user_id": "ashutosh",
                "user_type": "super-admin",
                "profile_pic": "",
                "entreprise_name": ""
            },
            {
                "question_id": 91,
                "question": "Testing 240",
                "topic_name": "Incubation Support",
                "publish_date": "2019-03-29 19:37:20",
                "likes": 1,
                "dislikes": 0,
                "count_comments": 4,
                "description": "testing 240 description",
                "slug": "testing-240",
                "first_name": "Kailash",
                "last_name": "Panigrahi",
                "user_id": "kailash-panigrahi",
                "user_type": "super-admin",
                "profile_pic": "1551352290fd456406745d816a45cae554c788e754.png",
                "entreprise_name": ""
            }
        ]
    }
}

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */


     public function getCategoryWiseQuestionsListing(Request $request,$slug)
     {
       $search = str_replace("%20"," ",$request->keyword);
       $limit = $request->input('limit',10);
       $offset = $request->input('offset',0);

       $query = ForumQuestions::select('forum_questions.id', 'categories.category_name as topic_name', 'forum.forum_name', 'forum_questions.forum_question', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.forum_question_slug','forum_questions.description','users.first_name','users.last_name','users.user_slug as user_id', 'users.user_type','user_profile.profile_pic','user_profile.gender')
       ->join('users','users.id','=','forum_questions.user_id')
       ->leftjoin('user_profile','users.id','=','user_profile.user_id')
       ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
       ->leftjoin('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
       ->join('categories','categories.id','=','forum_topics.topic_name')
       ->where(['forum_questions.deleted_at'=>NULL])
       ->where(['forum_questions.status'=>'Approved'])
       ->where('forum_topics.topic_slug',$slug)
       ->orderBy('forum_questions.updated_at','desc');
       if(isset($search) && !empty($search)) {
         $query =   $query->where('forum_questions.forum_question','like','%'.$search.'%');
       }

       if($request->sort == 'title') {
         if($request->order == 1){
           $query =  $query->orderBy('forum_questions.forum_question','ASC');
         } else {
           $query =  $query->orderBy('forum_questions.forum_question','DESC');
         }  
       }
       $count = $query->count();
       $question_list = $query
       ->limit($limit)
       ->offset($offset)
       ->get();

       $questions = array();
       if (isset($question_list) && count($question_list) > 0) {
         foreach ($question_list as $key1 => $value1) {
           $questions[$key1]['question_id'] = $value1->id;
           $questions[$key1]['question'] = $value1->forum_question;
           $questions[$key1]['topic_name'] = $value1->topic_name;
           $questions[$key1]['publish_date'] = $value1->publish_date;
           $questions[$key1]['likes'] = QuestionLikeMapper::where([
             'question_id' => $value1->id,
             'like_status' => 1,
           ])->count();

           $questions[$key1]['dislikes'] = QuestionLikeMapper::where([
             'question_id' => $value1->id,
             'like_status' => 0,
           ])->count();

           $questions[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$questions[$key1]['question_id'])->count();
           $questions[$key1]['description'] = substr($value1->description,0,200);
           $questions[$key1]['slug'] = $value1->forum_question_slug;
           $questions[$key1]['first_name'] = isset($value1->first_name)?$value1->first_name:"";
           $questions[$key1]['last_name'] = isset($value1->last_name)?$value1->last_name:"";
           $questions[$key1]['user_id'] = isset($value1->user_id)?$value1->user_id:"";
           $questions[$key1]['user_type'] = isset($value1->user_type)?$value1->user_type:"";
           $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:"";
           $questions[$key1]['entreprise_name'] = isset($value1->entreprise_name)?$value1->entreprise_name:"";
           if($value1->user_id && $value1->user_type && $value1->user_type == 'partner') {
             $slug = DB::table('partner_profile')->where('user_id',$value1->user_id)->first();
             $questions[$key1]['partner_slug'] = $slug ? $slug->slug:'';
             $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:(DB::table('partner_profile')->where('slug', $value1->user_id)->first()?DB::table('partner_profile')->where('slug', $value1->user_id)->first()->logo:'');
           }
         }
       } else {
         return $questions;
       }
       return new JsonResponse(['count'=> $count,'questions'=>$questions]);
     }
    /**
     * @api {get} /user-question/{slug} get user  specific question 
     * @apiName getUserQuestionSearch
     * @apiGroup admin
     * @apiParam {slug} search parameter
     * 
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
         "status_code": "2000",
        "message": "success",
        "body": {
        "count": 1,
        "questions": [
            {
                 "question_id": 26,
                "question": "tax",
                "topic_name": "Technology Validation",
                "publish_date": "2019-03-06 07:34:58",
                "likes": 0,
                "dislikes": 0,
                "count_comments": 0,
                "description": "....",
                "slug": "tax-3",
                "first_name": "WEP ADMIN",
                "last_name": "",
                "user_id": "ashutosh",
                "user_type": "super-admin",
                "profile_pic": "admin-pic.png",
                "entreprise_name": ""
            }
        ],
         }

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
     public function getUserQuestionSearch(Request $request, $slug) {
      $search = str_replace("%20"," ",$slug);
      if($request->has('limit')) {
        $limit = $request->get('limit');
      }else{
        $limit = 10;
      }
      if($request->has('offset')) {
        $offset = $request->get('offset');
      }else{
        $offset = 0;
      }
      $query = ForumQuestions::select('forum_questions.id', 'categories.category_name as topic_name', 'forum.forum_name', 'forum_questions.forum_question', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.forum_question_slug','forum_questions.description','users.first_name','users.last_name','users.user_slug as user_id', 'users.user_type','user_profile.profile_pic','user_profile.gender')
      ->join('users','users.id','=','forum_questions.user_id')
      ->leftjoin('user_profile','users.id','=','user_profile.user_id')
      ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
      ->leftjoin('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
      ->join('categories','categories.id','=','forum_topics.topic_name')
      ->where(['forum_questions.deleted_at'=>NULL,'forum_questions.user_id'=>$this->user_id,'forum_questions.status'=>'Approved'])
      ->where('forum_questions.forum_question','like','%'.$search.'%')
      ->orderBy('forum_questions.updated_at','desc');
      $question_list = $query
      ->limit($limit)
      ->offset($offset)
      ->get();
      
      $questions = array();
      if (isset($question_list) && count($question_list) > 0) {
        foreach ($question_list as $key1 => $value1) {
          $questions[$key1]['question_id'] = $value1->id;
          $questions[$key1]['question'] = $value1->forum_question;
          $questions[$key1]['topic_name'] = $value1->topic_name;
          $questions[$key1]['publish_date'] = $value1->publish_date;
          $questions[$key1]['likes'] = QuestionLikeMapper::where([
            'question_id' => $value1->id,
            'like_status' => 1,
          ])->count();

          $questions[$key1]['dislikes'] = QuestionLikeMapper::where([
            'question_id' => $value1->id,
            'like_status' => 0,
          ])->count();

          $questions[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$questions[$key1]['question_id'])->count();
          $questions[$key1]['description'] = substr($value1->description,0,200);
          $questions[$key1]['slug'] = $value1->forum_question_slug;
          $questions[$key1]['first_name'] = isset($value1->first_name)?$value1->first_name:"";
          $questions[$key1]['last_name'] = isset($value1->last_name)?$value1->last_name:"";
          $questions[$key1]['user_id'] = isset($value1->user_id)?$value1->user_id:"";
          $questions[$key1]['user_type'] = isset($value1->user_type)?$value1->user_type:"";
          $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:"";
          $questions[$key1]['entreprise_name'] = isset($value1->entreprise_name)?$value1->entreprise_name:"";
          if($value1->user_id && $value1->user_type && $value1->user_type == 'partner') {
            $slug = DB::table('partner_profile')->where('user_id',$value1->user_id)->first();
            $questions[$key1]['partner_slug'] = $slug ? $slug->slug:'';
            $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:(DB::table('partner_profile')->where('slug', $value1->user_id)->first()?DB::table('partner_profile')->where('slug', $value1->user_id)->first()->logo:'');
          }
        }

        if (count($questions) > 0) {
          return new JsonResponse(['count'=> $query->count(),'questions'=>$questions]);
        }
      } else {
        return $questions;
      }
    }

      /**
     * @api {get} /category-question/search/{slug}/{search} search question based on category
     * @apiName getCategoryQuestionSearch
     * @apiGroup admin
     *
     * @apiParam {slug} slug of category
     * @apiParam {search} parameter to be search
     * 
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
         "status_code": "2000",
        "message": "success",
        "body": {
        "count": 1,
        "questions": [
            {
                "question_id": 26,
                "question": "tax",
                "topic_name": "Technology Validation",
                "publish_date": "2019-03-06 07:34:58",
                "likes": 0,
                "dislikes": 0,
                "count_comments": 0,
                "description": "....",
                "slug": "tax-3",
                "first_name": "WEP ADMIN",
                "last_name": "",
                "user_id": "ashutosh",
                "user_type": "super-admin",
                "profile_pic": "admin-pic.png",
                "entreprise_name": ""
            }
        ],
         }

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
     public function getCategoryQuestionSearch(Request $request,$slug,$search) {

      $search = str_replace("%20"," ",$search);
      if($request->has('limit')) {
        $limit = $request->get('limit');
      }else{
        $limit = 10;
      }
      if($request->has('offset')) {
        $offset = $request->get('offset');
      }else{
        $offset = 0;
      }

      $query = ForumQuestions::select('forum_questions.id', 'categories.category_name as topic_name', 'forum.forum_name', 'forum_questions.forum_question', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.forum_question_slug','forum_questions.description','users.first_name','users.last_name','users.user_slug as user_id', 'users.user_type','user_profile.profile_pic','user_profile.gender')
      ->join('users','users.id','=','forum_questions.user_id')
      ->leftjoin('user_profile','users.id','=','user_profile.user_id')
      ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
      ->leftjoin('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
      ->join('categories','categories.id','=','forum_topics.topic_name')
      ->where(['forum_questions.deleted_at'=>NULL,'forum_questions.status'=>'Approved'])
      ->where('forum_questions.forum_question','like','%'.$search.'%')
      ->where('forum_topics.topic_slug',$slug)
      ->orderBy('forum_questions.updated_at','desc');
      $question_list = $query
      ->limit($limit)
      ->offset($offset)
      ->get();
      
      $questions = array();
      if (isset($question_list) && count($question_list) > 0) {
        foreach ($question_list as $key1 => $value1) {
          $questions[$key1]['question_id'] = $value1->id;
          $questions[$key1]['question'] = $value1->forum_question;
          $questions[$key1]['topic_name'] = $value1->topic_name;
          $questions[$key1]['publish_date'] = $value1->publish_date;
          $questions[$key1]['likes'] = QuestionLikeMapper::where([
            'question_id' => $value1->id,
            'like_status' => 1,
          ])->count();

          $questions[$key1]['dislikes'] = QuestionLikeMapper::where([
            'question_id' => $value1->id,
            'like_status' => 0,
          ])->count();

          $questions[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$questions[$key1]['question_id'])->count();
          $questions[$key1]['description'] = substr($value1->description,0,200);
          $questions[$key1]['slug'] = $value1->forum_question_slug;
          $questions[$key1]['first_name'] = isset($value1->first_name)?$value1->first_name:"";
          $questions[$key1]['last_name'] = isset($value1->last_name)?$value1->last_name:"";
          $questions[$key1]['user_id'] = isset($value1->user_id)?$value1->user_id:"";
          $questions[$key1]['user_type'] = isset($value1->user_type)?$value1->user_type:"";
          $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:"";
          $questions[$key1]['entreprise_name'] = isset($value1->entreprise_name)?$value1->entreprise_name:"";
          if($value1->user_id && $value1->user_type && $value1->user_type == 'partner') {
            $slug = DB::table('partner_profile')->where('user_id',$value1->user_id)->first();
            $questions[$key1]['partner_slug'] = $slug ? $slug->slug:'';
            $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:(DB::table('partner_profile')->where('slug', $value1->user_id)->first()?DB::table('partner_profile')->where('slug', $value1->user_id)->first()->logo:'');
          }
        }

        if (count($questions) > 0) {
          return new JsonResponse(['count'=> $query->count(),'questions'=>$questions]);
        }
      } else {
        return $questions;
      }
    }



    public function searchCommunity(Request $request,$search)
    {
      if($request->has('limit')) {
        $limit = $request->get('limit');
      }else{
        $limit = 10;
      }
      if($request->has('offset')) {
        $offset = $request->get('offset');
      }else{
        $offset = 0;
      }


      $question_list = DB::table('forum_questions')->select(DB::raw('DISTINCT (forum_questions.id)'), 'categories.category_name as topic_name', 'forum_questions.forum_question', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','users.display_name','user_profile.gender')
      ->join('users','users.id','=','forum_questions.user_id')
      ->leftjoin('user_profile','users.id','=','user_profile.user_id')
      ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
      ->join('categories','categories.id','=','forum_topics.topic_name')
      ->where(['forum_questions.deleted_at'=>NULL])
      ->where('forum_questions.forum_question','like','%'.$search.'%')
      ->where(['forum_questions.status'=>'Approved'])
      ->limit($limit)
      ->offset($offset)
      ->groupBy('forum_questions.id')
      ->orderBy('forum_questions.created_at','desc')
      ->get();
      
      $questions = array();
      if (isset($question_list) && count($question_list) > 0) {
        foreach ($question_list as $key1 => $value1) {
          $questions[$key1]['question_id'] = $value1->id;
          $questions[$key1]['question'] = $value1->forum_question;
          $questions[$key1]['topic_name'] = $value1->topic_name;
          $questions[$key1]['publish_date'] = $value1->publish_date;
          $questions[$key1]['likes'] = QuestionLikeMapper::where([
            'question_id' => $value1->id,
            'like_status' => 1,
          ])->count();

          $questions[$key1]['dislikes'] = QuestionLikeMapper::where([
            'question_id' => $value1->id,
            'like_status' => 0,
          ])->count();

          $questions[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$questions[$key1]['question_id'])->count();
          $questions[$key1]['description'] = substr($value1->description,0,200);
          $questions[$key1]['slug'] = $value1->forum_question_slug;
          $questions[$key1]['first_name'] = isset($value1->first_name)?$value1->first_name:"";
          $questions[$key1]['last_name'] = isset($value1->last_name)?$value1->last_name:"";
          $questions[$key1]['user_id'] = isset($value1->user_id)?$value1->user_id:"";
          $questions[$key1]['user_type'] = isset($value1->user_type)?$value1->user_type:"";
          $questions[$key1]['display_name'] = isset($value1->display_name)?$value1->display_name:"";
          $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:"";
          $questions[$key1]['entreprise_name'] = isset($value1->entreprise_name)?$value1->entreprise_name:"";
          if($value1->user_id && $value1->user_type && $value1->user_type == 'partner') {
            $slug = DB::table('partner_profile')->where('user_id',$value1->user_id)->first();
            $questions[$key1]['partner_slug'] = $slug ? $slug->slug:'';
            $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:(DB::table('partner_profile')->where('slug', $value1->user_id)->first()?DB::table('partner_profile')->where('slug', $value1->user_id)->first()->logo:'');
          }
        }

        if (count($questions) > 0) {
          return $questions;
        }
      } else {
        return $questions;
      }
    }

  //UserFeed 
    // public function getUserFeed(Request $request) {

    //   $user_id = $this->user_id;
    //   $data['count']= DB::select("select count(forum_questions.id) as count FROM forum_questions INNER JOIN 
    //     categories ON forum_questions.topic_id = categories.id 
    //     INNER JOIN entrepreneur_interest_area ON entrepreneur_interest_area.interest_area_id =categories.id where entrepreneur_interest_area.user_id =".$user_id." And forum_questions.status='approved' And entrepreneur_interest_area.deleted_at is NULL And forum_questions.deleted_at is NULL");
    //   // $data['data'] =  DB::select("select forum_questions.*,users.first_name,users.last_name,users.user_slug as user_id, users.user_type,user_profile.profile_pic,users.display_name,user_profile.gender FROM forum_questions  
    //   //   INNER JOIN categories ON forum_questions.topic_id = categories.id  
    //   //   INNER JOIN users on users.id = forum_questions.user_id 
    //   //   LEFT JOIN user_profile ON users.id = user_profile.user_id
    //   //   INNER JOIN entrepreneur_interest_area ON entrepreneur_interest_area.interest_area_id =categories.id
    //   //   where entrepreneur_interest_area.user_id =".$user_id." And 
    //   //   forum_questions.status='approved' And 
    //   //   entrepreneur_interest_area.deleted_at is NULL  And 
    //   //   forum_questions.deleted_at is NULL order by forum_questions.id desc limit 5");

    //   $data['data']=ForumQuestions::select('forum_questions.*','users.first_name','users.last_name','users.user_slug as user_id', 'users.user_type','user_profile.profile_pic','users.display_name','user_profile.gender')
    //   ->join('categories','forum_questions.topic_id','=', 'categories.id')
    //   ->join('users', 'users.id', '=' ,'forum_questions.user_id')
    //   ->leftjoin('user_profile','users.id', '=', 'user_profile.user_id')
    //   ->join('entrepreneur_interest_area','entrepreneur_interest_area.interest_area_id' ,'=','categories.id')
    //   ->where(['entrepreneur_interest_area.user_id'=>$user_id,'forum_questions.status'=>'approved','entrepreneur_interest_area.deleted_at' => null,'forum_questions.deleted_at' =>null])
    //   ->orderBy('forum_questions.id','desc')
    //   ->limit(5)->get();


    //  // $data['count_data'] = $data['data'][0]->id; 


    //  // $questions[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$questions[$key1]['question_id'])->count();

    //   return $data;
    // }

    public function getUserFeed(Request $request) {

      $user_id = $this->user_id;
     $question_list = DB::table('forum_questions')->select(DB::raw('DISTINCT (forum_questions.forum_question)','forum_questions.id'), 'forum_questions.id','forum_questions.forum_question', 'categories.category_name as topic_name', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','users.display_name','user_profile.gender','user_forum_like_mapper.like_status')
      ->join('users','users.id','=','forum_questions.user_id')
      ->leftjoin('user_profile','users.id','=','user_profile.user_id')
      ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
      ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
      ->join('categories','categories.id','=','forum_topics.topic_name')
      ->leftJoin('user_forum_like_mapper', function($join) {
        $join->on('user_forum_like_mapper.question_id','=','forum_questions.id');
        $join->where('user_forum_like_mapper.user_id', '=',$this->user_id);
      })
      ->join('entrepreneur_interest_area','entrepreneur_interest_area.interest_area_id' ,'=','categories.id')
      ->where(['forum_questions.deleted_at'=>NULL])
      ->where(['forum_questions.status'=>'Approved'])
      ->where('entrepreneur_interest_area.user_id',$user_id)
      ->groupBy('forum_questions.id')
      ->orderBy('forum_questions.id','desc')
      ->limit(5)
      ->get();


        $questions = array();
    if (isset($question_list) && count($question_list) > 0) {
      foreach ($question_list as $key1 => $value1) {
        $data[$key1]['question_id'] = $value1->id;
        $data[$key1]['question'] = $value1->forum_question;
        $data[$key1]['topic_name'] = $value1->topic_name;
        $data[$key1]['publish_date'] = $value1->publish_date;
        $data[$key1]['likes'] = QuestionLikeMapper::where([
          'question_id' => $value1->id,
          'like_status' => 1,
        ])->count();
      
        $data[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$data[$key1]['question_id'])->count();
       // $questions[$key1]['description'] = substr($value1->description,0,200);
        $data[$key1]['description'] = $value1->description;
        $data[$key1]['slug'] = $value1->forum_question_slug;
        $data[$key1]['first_name'] = isset($value1->first_name)?$value1->first_name:"";
        $data[$key1]['last_name'] = isset($value1->last_name)?$value1->last_name:"";
        $data[$key1]['gender'] = isset($value1->gender)?$value1->gender:"";
        $data[$key1]['user_id'] = isset($value1->user_id)?$value1->user_id:"";
        $data[$key1]['user_type'] = isset($value1->user_type)?$value1->user_type:"";
        $data[$key1]['display_name'] = isset($value1->display_name)?$value1->display_name:"";
        $data[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:"";
        $data[$key1]['entreprise_name'] = isset($value1->entreprise_name)?$value1->entreprise_name:"";
        $data[$key1]['like_status'] = isset($value1->like_status)?$value1->like_status:"";
        if($value1->user_id && $value1->user_type && $value1->user_type == 'partner') {
          $slug = DB::table('partner_profile')->where('user_id',$value1->user_id)->first();
          $data[$key1]['partner_slug'] = $slug ? $slug->slug:'';
          $data[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:(DB::table('partner_profile')->where('slug', $value1->user_id)->first()?DB::table('partner_profile')->where('slug', $value1->user_id)->first()->logo:'');
        }
      }

      if (count($data) > 0) {
        return $data['data'] = $data;
      }
    } else {
      return $data;
    }


     // $data['count_data'] = $data['data'][0]->id; 


     // $questions[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$questions[$key1]['question_id'])->count();

    }


    /**
     * @api {get} /userwise-question-answer My forum
     * @apiName Get Own question and answer
     * @apiGroup admin
     * @apiParam {search} search search forum question, answers and inner comments
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
   {
    "status_code": "2000",
    "message": "success",
    "body": {
        "current_page": 1,
        "data": [
            {
                "forum_question": "Financial Assistance",
                "forum_question_slug": "financial-assistance",
                "answers": "sdklasmdasd",
                "main_answer": null,
                "answer_created_at": "2019-11-21 15:18:14",
                "question_created_at": "2020-03-04 10:31:06"
            },
            {
                "forum_question": "Financial Assistance",
                "forum_question_slug": "financial-assistance",
                "answers": "inner comment",
                "main_answer": "sdklasmdasd",
                "answer_created_at": "2020-04-10 18:21:06",
                "question_created_at": "2020-03-04 10:31:06"
            }
        ],
        "first_page_url": "http://wepcommunity.choicetechlab.com/api/community/userwise-question-answer?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://wepcommunity.choicetechlab.com/api/community/userwise-question-answer?page=1",
        "next_page_url": null,
        "path": "http://wepcommunity.choicetechlab.com/api/community/userwise-question-answer",
        "per_page": 10,
        "prev_page_url": null,
        "to": 2,
        "total": 2
    }
}

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
     public function getForumQuestionAnswerUserWise(Request $request)
     {

      $limit = $request->limit?$request->limit:10;

      $questions = DB::table('forum_thread as ft')->select(DB::raw('DISTINCT(forum_questions.id)as forum_questions_id'),"forum_questions.forum_question",'forum_questions.forum_question_slug','ft.thread_answer as main_answer',DB::raw('GROUP_CONCAT(CONCAT_WS("-",ft.id,ft.thread_answer,ft.likes,user_forum_like_mapper.like_status,IFNULL(ft.parent_id,"")) SEPARATOR "|") as answers'),'ft.created_at as answer_created_at','forum_questions.created_at as question_created_at','users.first_name','users.last_name','users.user_slug as user_id', 'users.user_type','user_profile.profile_pic','user_profile.gender')
      ->leftjoin('forum_questions','forum_questions.id','=','ft.question_id')
      ->leftjoin('users','users.id','=','forum_questions.user_id')
      ->join('user_profile','users.id','=','user_profile.user_id')
      ->leftjoin('user_forum_like_mapper', function($join) {
        $join->on('user_forum_like_mapper.question_id','=','forum_questions.id');
        $join->where('user_forum_like_mapper.user_id', '=',$this->user_id);
      })
      ->leftjoin('forum_thread as ft2','ft2.id','=','ft.question_id')
      ->where('ft.user_id','=', $this->user_id)
      ->groupBy('ft.question_id')
      ->orderBy('forum_questions.id','desc');
      if(isset($request->search)) {
       $questions = $questions->where('forum_questions.forum_question','like','%'.$request->search.'%')
       ->orWhere('ft.thread_answer','like','%'.$request->search.'%');
     }
     $questions = $questions->get();
     $questions_list=[];
     $last_key_val=0;
     foreach ($questions as $key1 => $value1) {
      $answers = explode('|',$value1->answers);
      $questions_list[$key1]['forum_question_id'] = $value1->forum_questions_id;
      $questions_list[$key1]['forum_question'] = $value1->forum_question;
      $questions_list[$key1]['forum_question_slug'] = $value1->forum_question_slug;
      $questions_list[$key1]['first_name'] = $value1->first_name;
      $questions_list[$key1]['last_name'] = $value1->last_name;
      $questions_list[$key1]['user_id'] = $value1->user_id;
      $questions_list[$key1]['user_type'] = $value1->user_type;
      $questions_list[$key1]['profile_pic'] = $value1->profile_pic;
      $questions_list[$key1]['gender'] = $value1->gender;
      $questions_list[$key1]['answer_created_at'] = $value1->answer_created_at;
      $questions_list[$key1]['question_created_at'] = $value1->question_created_at;
      foreach ($answers as $key2 => $value2) {
       $ans = explode('-',$value2);
       $answ[$key2]['thread']['id'] = isset($ans[0])?$ans[0]:"";
       $answ[$key2]['thread']['reply'] = isset($ans[1])?$ans[1]:"";
       $answ[$key2]['thread']['like_count'] = isset($ans[2])?$ans[2]:"";
       $answ[$key2]['thread']['like_status'] = isset($ans[3])?$ans[3]:"";
       $answ[$key2]['thread']['parent_id'] = isset($ans[4])?$ans[4]:"";
     }
     $questions_list[$key1]['ans_threads'] = $answ; 
   }
   return $questions_list;
 }

 public function myComments(Request $request)
 {
  if($request->has('limit')) {
    $limit = $request->get('limit');
  }else{
    $limit = 10;
  }
  if($request->has('offset')) {
    $offset = $request->get('offset');
  }else{
    $offset = 0;
  }

  $question_list = DB::table('forum_questions')->select(DB::raw('DISTINCT(forum_questions.id)','forum_questions.id'),'forum_questions.forum_question', 'categories.category_name as topic_name', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','users.display_name','user_profile.gender','user_forum_like_mapper.like_status')
  ->join('users','users.id','=','forum_questions.user_id')
  ->leftjoin('user_profile','users.id','=','user_profile.user_id')
  ->leftjoin('forum','forum.id','=','forum_questions.forum_id')
  ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
  ->join('categories','categories.id','=','forum_topics.topic_name')
  ->join('forum_thread','forum_thread.question_id','=','forum_questions.id')
  ->leftJoin('user_forum_like_mapper', function($join) {
    $join->on('user_forum_like_mapper.question_id','=','forum_questions.id');
    $join->where('user_forum_like_mapper.user_id', '=',$this->user_id);
  })
  ->where(['forum_questions.deleted_at'=>NULL,'forum_thread.deleted_at'=>NULL,'categories.deleted_at'=>NULL,
    'users.deleted_at'=>NULL,'user_profile.deleted_at'=>NULL,'user_forum_like_mapper.deleted_at'=>NULL])
  ->where(['forum_questions.status'=>'Approved'])
  ->where('forum_thread.user_id',$this->user_id);

  if(isset($request->keyword) && !empty($request->keyword)) {

   $question_list = $question_list->where('forum_questions.forum_question','like','%'.$request->keyword.'%');
 }

 $count = $question_list->groupBy('forum_questions.id')->orderBy('forum_questions.created_at','desc')->get();

 $question_list = $question_list->limit($limit)
 ->offset($offset)
 ->groupBy('forum_questions.id')
 ->orderBy('forum_thread.created_at','desc')
 ->get();

 $questions = array();
 if (isset($question_list) && count($question_list) > 0) {
  foreach ($question_list as $key1 => $value1) {
    $questions[$key1]['question_id'] = $value1->id;
    $questions[$key1]['question'] = $value1->forum_question;
    $questions[$key1]['topic_name'] = $value1->topic_name;
    $questions[$key1]['publish_date'] = $value1->publish_date;
    $questions[$key1]['likes'] = QuestionLikeMapper::where([
      'question_id' => $value1->id,
      'like_status' => 1,
    ])->count();
    $questions[$key1]['dislikes'] = QuestionLikeMapper::where([
      'question_id' => $value1->id,
      'like_status' => 0,
    ])->count();

    $questions[$key1]['count_comments'] = ForumThread::select('forum_thread.id')->where('question_id',$questions[$key1]['question_id'])->count();
       // $questions[$key1]['description'] = substr($value1->description,0,200);
    $questions[$key1]['description'] = $value1->description;
    $questions[$key1]['slug'] = $value1->forum_question_slug;
    $questions[$key1]['first_name'] = isset($value1->first_name)?$value1->first_name:"";
    $questions[$key1]['last_name'] = isset($value1->last_name)?$value1->last_name:"";
    $questions[$key1]['gender'] = isset($value1->gender)?$value1->gender:"";
    $questions[$key1]['user_id'] = isset($value1->user_id)?$value1->user_id:"";
    $questions[$key1]['user_type'] = isset($value1->user_type)?$value1->user_type:"";
    $questions[$key1]['display_name'] = isset($value1->display_name)?$value1->display_name:"";
    $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:"";
    $questions[$key1]['entreprise_name'] = isset($value1->entreprise_name)?$value1->entreprise_name:"";
    $questions[$key1]['like_status'] = isset($value1->like_status)?$value1->like_status:"";
    if($value1->user_id && $value1->user_type && $value1->user_type == 'partner') {
      $slug = DB::table('partner_profile')->where('user_id',$value1->user_id)->first();
      $questions[$key1]['partner_slug'] = $slug ? $slug->slug:'';
      $questions[$key1]['profile_pic'] = isset($value1->profile_pic)?$value1->profile_pic:(DB::table('partner_profile')->where('slug', $value1->user_id)->first()?DB::table('partner_profile')->where('slug', $value1->user_id)->first()->logo:'');
    }
  }

  if (count($questions) > 0) {
   return new JsonResponse(['count'=> count($count),'questions'=>$questions]);
       // return $questions;
 }
} else {
  return $questions;
}
}

}
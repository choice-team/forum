<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ForumTopics;
use Illuminate\Support\Facades\Auth;
use App\Models\GlobalSearch;

class ExampleController extends Controller
{
  

    public function search(Request $request,$search_topic = 'all', $search_keyword = null ) {
    
        $search_keyword = isset($request->search_keyword)?$request->search_keyword:"";
        $category = isset($request->search_category)?$request->search_category:"";
        $limit = isset($request->limit)?$request->limit:10;
        $offset = isset($request->offset)? $request->offset:0;
    
        $this->validate($request, [
           // 'search_keyword' => 'required|string',
            'search_category'=> 'required|string',
            'limit' => 'nullable|numeric|min:10|max:100',
            'offset' => 'nullable|numeric|min:0|max:10000'
        ]);
        $search_results = [];
        if ($request->search_category == 'all') {
            // $search_results['keyword'] = $search_keyword;
            try {
                $count = DB::table('forum_questions')
                ->join('users','users.id','=','forum_questions.user_id')
                ->leftjoin('user_profile','users.id','=','user_profile.user_id')
                ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
                ->join('categories','categories.id','=','forum_topics.topic_name')
                        ->where('forum_question', 'LIKE', "%{$search_keyword}%")
                        ->where(['forum_questions.deleted_at'=>NULL])
                        ->skip($offset)
                        ->take($limit)
                        ->count();
                $results = DB::table('forum_questions')
                ->join('users','users.id','=','forum_questions.user_id')
                ->leftjoin('user_profile','users.id','=','user_profile.user_id')
                ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
                ->join('categories','categories.id','=','forum_topics.topic_name')
                        ->where('forum_question', 'LIKE', "%{$search_keyword}%")
                        ->where(['forum_questions.deleted_at'=>NULL])
                        ->skip($offset)
                        ->take($limit)
                        ->get([DB::raw('DISTINCT (forum_questions.id)'), 'categories.category_name as topic_name', 'forum_questions.forum_question', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','users.display_name']);
            } catch (Exception $exc) {
                return array($exc->getMessage());
            }
        }else if ($request->search_category == $category){
            // $search_results['inside_category'] = $search_keyword;
            try {
               $count = DB::table('forum_questions')
               ->join('users','users.id','=','forum_questions.user_id')
               ->leftjoin('user_profile','users.id','=','user_profile.user_id')
               ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
               ->join('categories','categories.id','=','forum_topics.topic_name')
                       ->whereRaw('topic_id IN (SELECT topic_name from forum_topics WHERE topic_slug = ?)', $category)
                       ->where('forum_question', 'LIKE', "%{$search_keyword}%")
                       ->where(['forum_questions.deleted_at'=>NULL])
                       ->skip($offset)
                       ->take($limit)
                       ->count();
                $results = DB::table('forum_questions')
                ->join('users','users.id','=','forum_questions.user_id')
                ->leftjoin('user_profile','users.id','=','user_profile.user_id')
                ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
                ->join('categories','categories.id','=','forum_topics.topic_name')
                        ->whereRaw('topic_id IN (SELECT topic_name from forum_topics WHERE topic_slug = ?)', $category)
                        ->where('forum_question', 'LIKE', "%{$search_keyword}%")
                        ->where(['forum_questions.deleted_at'=>NULL])
                        ->skip($offset)
                        ->take($limit)
                        ->get([DB::raw('DISTINCT (forum_questions.id)'), 'categories.category_name as topic_name', 'forum_questions.forum_question', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','users.display_name']);
            } catch (Exception $exc) {
                return array($exc->getMessage());
            }
        } 

        else {
            try {
                $count = DB::table('forum_questions')
                ->join('users','users.id','=','forum_questions.user_id')
                ->leftjoin('user_profile','users.id','=','user_profile.user_id')
                ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
                ->join('categories','categories.id','=','forum_topics.topic_name')
                        ->whereRaw('topic_id IN (SELECT topic_name from forum_topics WHERE topic_slug = ?)', $category)
                        ->where('forum_question', 'LIKE', "%{$search_keyword}%")
                        ->where(['forum_questions.deleted_at'=>NULL])
                        ->skip($offset)
                        ->take($limit)
                        ->count();
                $results = DB::table('forum_questions')
                ->join('users','users.id','=','forum_questions.user_id')
                ->leftjoin('user_profile','users.id','=','user_profile.user_id')
                ->join('forum_topics','forum_topics.topic_name','=','forum_questions.topic_id')
                ->join('categories','categories.id','=','forum_topics.topic_name')
                        ->whereRaw('topic_id IN (SELECT topic_name from forum_topics WHERE topic_slug = ?)', $category)
                        ->where('forum_question', 'LIKE', "%{$search_keyword}%")
                        ->where(['forum_questions.deleted_at'=>NULL])
                        ->skip($offset)
                        ->take($limit)
                        ->get([DB::raw('DISTINCT (forum_questions.id)'), 'categories.category_name as topic_name', 'forum_questions.forum_question', 'forum_questions.user_id','forum_questions.likes','forum_questions.dislikes','forum_questions.created_at as publish_date','forum_questions.status','forum_questions.description','forum_questions.forum_question_slug','users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as user_id','user_profile.entreprise_name', 'users.user_type','users.display_name']);
            } catch (Exception $exc) {
                return array($exc->getMessage());
            }
        }
        $search_results['results_count'] = $count;
        $search_results['results_data'] = $results;
        return $search_results;
  }


  public function addModule(Request $request) {
     $category =  ForumTopics::where('id','<',24)->get()->pluck('topic_slug')->toArray();
     foreach($category as $value) {
        $flag = DB::table('modules')->where('slug',$value)->first();
        if(!$flag) {
            $shortform = (new self)->generateShortform($value);
            $id = DB::table('modules')->insertGetId(['name'=>trim($value),'parent_id'=>12,'slug'=>trim($value),'shortform'=>$shortform]);
            $operations = DB::table('operations')->get()->pluck('name','id');
            foreach($operations as $k=>$v) {
                DB::table('permissions')->insert(['slug'=>(trim($value).'-'.$v),'module_id'=>$id,'operation_id'=>$k]);
            }
        }
     }  
  }

  protected function generateShortform($name) {
    if(preg_match_all('/\b(\w)/',strtolower($name),$m)) {
        $v = implode('',$m[1]); // $v is now SOQTU
    }
    return $v;
 }

 public function globalSearch(Request $request,$search_topic = 'all', $search_keyword = null){
   
        $search_keyword = $request['search_keyword'] = urldecode($search_keyword);
        $category = $request['search_category'] = urldecode($search_topic);
        $request['limit'] = ($request['limit']) ? $request['limit'] : 10;
        $request['offset'] = ($request['offset']) ? $request['offset'] : 0;
        $this->validate($request, [
            'search_keyword' => 'required|string',
            'limit' => 'nullable|numeric|max:100',
            'offset' => 'nullable|numeric|max:10000'
        ]);
        $search_results = [];
        if ($request['search_category'] == 'all') {
            try {
                $results = DB::table('forum_questions')
                        ->where('forum_question', 'LIKE', "%{$search_keyword}%")
                        ->where(['forum_questions.deleted_at'=>NULL])
                        // ->skip($request['offset'])
                        // ->take($request['limit'])
                        ->get(['id', 'forum_question as search_title', 'forum_question_slug as search_slug', 'description as search_excerpt']);
            } catch (Exception $exc) {
                return array($exc->getMessage());
            }
        } else {
            try {
                $results = DB::table('forum_questions')
                        ->whereRaw('topic_id IN (SELECT id from forum_topics WHERE topic_slug = ?)', $category)
                        ->where('forum_question', 'LIKE', "%{$search_keyword}%")
                        ->where(['forum_questions.deleted_at'=>NULL])
                        // ->skip($request['offset'])
                        // ->take($request['limit'])
                        ->get(['id', 'forum_question as search_title', 'forum_question_slug as search_slug', 'description as search_excerpt']);
            } catch (Exception $exc) {
                return array($exc->getMessage());
            }
        }
        $search_results['topic'] = $category;
        $search_results['search_keyword'] = $search_keyword;
        $search_results['results_offset'] = $request->query('offset');
        $search_results['results_limit'] = $request->query('limit');
        $search_results['results_count'] = $results->count();
        $search_results['results_data'] = $results;

        $search_results_json =  json_encode($search_results);
        $id = 0;
        if(Auth::check()) {
          $id = Auth::user()->id;
        } 

        $actLog = GlobalSearch::create([
          'category' => 'community',
          'search_keyword' => $search_keyword,
          'search_results' => $search_results_json,
          'user_id' => $id
        ]);
        return $search_results;
  }

 
  
     
}

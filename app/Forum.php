<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Forum extends Model
{
    use SoftDeletes, LogsActivity;
    protected static $logFillable = true;
    protected $table = 'forum';

    public $fillable = ['forum_name','forum_description','forum_image','user_id','created_at','updated_at', 'deleted_at'];
}

<?php

namespace App;

use App\ForumQuestions;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumThread extends Model
{
    use SoftDeletes, LogsActivity;
    protected static $logFillable = true;
    protected $table = 'forum_thread';

    public $fillable = ['question_id','thread_answer','parent_id','user_id','likes','created_at','updated_at', 'deleted_at'];

    public function question()
    {
    	return $this->belongsTo(ForumQuestions::class);
    }
}

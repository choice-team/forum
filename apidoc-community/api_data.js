define({ "api": [
  {
    "type": "post",
    "url": "{base_url}/api/community/thread",
    "title": "Add Answer / Comment",
    "name": "Add_Answer_to_Community_Questions",
    "group": "Community",
    "description": "<p>To store answer in community questions</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>api token key.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "forum_question_slug",
            "description": "<p>forum_question_slug pass question slug to add answer for particular question</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "thread_answer",
            "description": "<p>thread_answer add answer</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n  {\n  \"status_code\":\"2000\",\n  \"message\":\"success\",\n  \"body\":{\n      \"last_id\":999\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "Community"
  },
  {
    "type": "Post",
    "url": "{base_url}/api/community/thread/like",
    "title": "Like Answer",
    "name": "Add_Like_to_Answer_in_Community_Questions",
    "group": "Community",
    "description": "<p>To add like answer in community questions</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>api token key.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "like_status",
            "description": "<p>like_status it is set to 1 to increase the like count</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "thread_id",
            "description": "<p>thread_id answer to be liked for particular questions id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n  {\n    \"status_code\":\"2000\",\n    \"message\":\"success\",\n    \"body\":{\n        \"insert_value\":105,\n        \"updated_likes_counter\":1,\n        \"like_status\":\"1\"\n    }\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "Community"
  },
  {
    "type": "GET",
    "url": "{base_url}/api/community/question?limit={limit}&offset={offset}&answered=0&likes=0",
    "title": "Questions",
    "name": "Community_Questions",
    "group": "Community",
    "description": "<p>To get community questions</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>api token key.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "limit",
            "description": "<p>set to limit the filter results</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "offset",
            "description": "<p>set to offset the filter results to a particular record count</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "answered",
            "description": "<p>answered passed as 0 for Recently added question and likes,for Popular : answered = 0 and for likes = 1 , for Answered question :  answered   = 1 and likes = 0</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "likes",
            "description": "<p>likes passed as 0 for Recently added question</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "    HTTP/1.1 2000 OK\n    {\n   \"status_code\":\"2000\",\n   \"message\":\"success\",\n   \"body\":[\n      {\n         \"question_id\":225,\n         \"question\":\"test\",\n         \"topic_name\":\"Incubation Support\",\n         \"publish_date\":\"2020-01-29 16:39:13\",\n         \"likes\":0,\n         \"dislikes\":0,\n         \"count_comments\":0,\n         \"description\":\"test\",\n         \"slug\":\"test-3\",\n         \"first_name\":\"Wep\",\n         \"last_name\":\"Niti Aayog\",\n         \"user_id\":\"wep-niti-aayog\",\n         \"user_type\":\"super-admin\",\n         \"display_name\":\"Niti Aayog\",\n         \"profile_pic\":\"\",\n         \"entreprise_name\":\"\"\n      },\n      {\n         \"question_id\":210,\n         \"question\":\"qeqwee1233\",\n         \"topic_name\":\"Business Validation\",\n         \"publish_date\":\"2020-01-01 16:14:38\",\n         \"likes\":1,\n         \"dislikes\":0,\n         \"count_comments\":0,\n         \"description\":\"ewfee\",\n         \"slug\":\"qeqwee1233\",\n         \"first_name\":\"Wep\",\n         \"last_name\":\"Niti Aayog\",\n         \"user_id\":\"wep-niti-aayog\",\n         \"user_type\":\"super-admin\",\n         \"display_name\":\"Niti Aayog\",\n         \"profile_pic\":\"\",\n         \"entreprise_name\":\"\"\n      }\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "Community"
  },
  {
    "type": "GET",
    "url": "{base_url}/api/community/thread/{slug}?limit={limit}&offset={limit}",
    "title": "Thread",
    "name": "Question_Thread",
    "group": "Community",
    "description": "<p>To view community questions  threads</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>api token key.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>slug slug of question</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "limit",
            "description": "<p>set to limit the filter results</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "offset",
            "description": "<p>set to offset the filter results to a particular record count</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "    HTTP/1.1 2000 OK\n   {\n   \"status_code\":\"2000\",\n   \"message\":\"success\",\n   \"body\":{\n      \"count\":1,\n      \"thread\":[\n         {\n            \"id\":997,\n            \"question_id\":211,\n            \"forum_question\":\"Duplicate check for registration know how\",\n            \"likes\":0,\n            \"user_slug\":\"wep-niti-aayog\",\n            \"user_type\":\"super-admin\",\n            \"first_name\":\"Wep\",\n            \"parent_id\":null,\n            \"last_name\":\"Niti Aayog\",\n            \"publish_date\":{\n               \"date\":\"2020-01-29 19:26:46.000000\",\n               \"timezone_type\":3,\n               \"timezone\":\"Asia\\/Kolkata\"\n            },\n            \"display_name\":\"Niti Aayog\",\n            \"like_status\":0,\n            \"profile_pic\":null,\n            \"entreprise_name\":null,\n            \"thread_answer\":\"demo\\n\",\n            \"thread_count\":0\n         }\n      ]\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "Community"
  },
  {
    "type": "GET",
    "url": "{base_url}/api/community/top-contributor",
    "title": "Top Contributor",
    "name": "Top_Contributor_in_Community",
    "group": "Community",
    "description": "<p>To get community questions</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>api token key.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "limit",
            "description": "<p>set to limit the filter results</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "offset",
            "description": "<p>set to offset the filter results to a particular record count</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n  {\n  \"status_code\":\"2000\",\n  \"message\":\"success\",\n  \"body\":[\n      {\n        \"first_name\":\"GEN India\",\n        \"last_name\":\"\",\n        \"user_slug\":\"gen-india\",\n        \"count\":51,\n        \"profile_pic\":\"partner-gen-india.png\",\n        \"entreprise_name\":null,\n        \"user_type\":\"partner\"\n      },\n      {\n        \"first_name\":\"Sanity\",\n        \"last_name\":\"Test\",\n        \"user_slug\":\"sanity-test\",\n        \"count\":27,\n        \"profile_pic\":\"1549613237a94c684474f600c1d46b24fd8d16b2a4.png\",\n        \"entreprise_name\":null,\n        \"user_type\":\"entrepreneur\"\n      }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "Community"
  },
  {
    "type": "GET",
    "url": "{base_url}/api/community/question/{slug}",
    "title": "View Question",
    "name": "View_Community_Question",
    "group": "Community",
    "description": "<p>To view community questions</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>api token key.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>slug slug of question</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "    HTTP/1.1 2000 OK\n    {\n   \"status_code\":\"2000\",\n   \"message\":\"success\",\n   \"body\":{\n      \"question\":[\n         {\n            \"id\":211,\n            \"topic_id\":14,\n            \"forum_question\":\"Duplicate check for registration know how\",\n            \"forum_question_slug\":\"duplicate-check-for-registration-know-how\",\n            \"description\":\"erreg\",\n            \"likes\":2,\n            \"dislikes\":1,\n            \"posted_date\":\"0000-00-00\",\n            \"publish_date\":\"2020-01-01 16:15:57\",\n            \"status\":\"Approved\",\n            \"topic_name\":14,\n            \"topic_slug\":\"registration-know-how\",\n            \"forum_name\":null,\n            \"first_name\":\"Wep\",\n            \"last_name\":\"Niti Aayog\",\n            \"profile_pic\":null,\n            \"user_id\":\"wep-niti-aayog\",\n            \"entreprise_name\":null,\n            \"like_status\":1,\n            \"user_type\":\"super-admin\",\n            \"display_name\":\"Niti Aayog\"\n         }\n      ],\n      \"suggested_list\":[\n         {\n            \"id\":140,\n            \"topic_id\":14,\n            \"forum_question\":\"reg know-how\",\n            \"forum_question_slug\":\"reg-know-how\",\n            \"description\":\"reg know-how\\nreg know-how\\nreg know-how\\nreg know-how\",\n            \"likes\":0,\n            \"dislikes\":0,\n            \"posted_date\":\"0000-00-00\",\n            \"publish_date\":\"2019-05-13 17:06:03\",\n            \"status\":\"Approved\",\n            \"topic_name\":null,\n            \"forum_name\":null,\n            \"first_name\":\"Tester\",\n            \"last_name\":\"p\",\n            \"profile_pic\":\"1546603262422fc48b309a8e76c8c00cd1b0853601.jpg\",\n            \"user_id\":\"tester-p\",\n            \"entreprise_name\":null,\n            \"user_type\":\"entrepreneur\"\n         }\n      ],\n      \"questions\":[\n         {\n            \"thread_count\":0\n         }\n      ]\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "Community"
  },
  {
    "type": "patch",
    "url": "{base_url}/api/community/delete-question/{slug}",
    "title": "Delete Question",
    "name": "Delete_Community_question",
    "group": "Dashboard_Community",
    "description": "<p>To delete community questions category wise.</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>api token key.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>pass forum_topic as slug to be deleted</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n{\n  \"status_code\":\"2000\",\n  \"message\":\"success\",\n  \"body\":[\n\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "Dashboard_Community"
  },
  {
    "type": "post",
    "url": "{base_url}/api/community/forum/{id}",
    "title": "Delete",
    "name": "Delete_forum_question",
    "group": "My_Forum",
    "description": "<p>To delete forum question</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>api token key.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n {\n  \"status_code\":\"2000\",\n  \"message\":\"success\",\n  \"body\":[\n\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "My_Forum"
  },
  {
    "type": "post",
    "url": "{base_url}/api/community/userwise-question-answer",
    "title": "My forum",
    "name": "Get_Own_question_and_answer",
    "group": "My_Forum",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "search",
            "optional": false,
            "field": "search",
            "description": "<p>search forum question, answers and inner comments</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "     HTTP/1.1 2000 OK\n   {\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"current_page\": 1,\n        \"data\": [\n            {\n                \"forum_question\": \"Financial Assistance\",\n                \"forum_question_slug\": \"financial-assistance\",\n                \"answers\": \"sdklasmdasd\",\n                \"main_answer\": null,\n                \"answer_created_at\": \"2019-11-21 15:18:14\",\n                \"question_created_at\": \"2020-03-04 10:31:06\"\n            },\n            {\n                \"forum_question\": \"Financial Assistance\",\n                \"forum_question_slug\": \"financial-assistance\",\n                \"answers\": \"inner comment\",\n                \"main_answer\": \"sdklasmdasd\",\n                \"answer_created_at\": \"2020-04-10 18:21:06\",\n                \"question_created_at\": \"2020-03-04 10:31:06\"\n            }\n        ],\n        \"first_page_url\": \"http://wepcommunity.choicetechlab.com/api/community/userwise-question-answer?page=1\",\n        \"from\": 1,\n        \"last_page\": 1,\n        \"last_page_url\": \"http://wepcommunity.choicetechlab.com/api/community/userwise-question-answer?page=1\",\n        \"next_page_url\": null,\n        \"path\": \"http://wepcommunity.choicetechlab.com/api/community/userwise-question-answer\",\n        \"per_page\": 10,\n        \"prev_page_url\": null,\n        \"to\": 2,\n        \"total\": 2\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "My_Forum"
  },
  {
    "type": "get",
    "url": "{base_url}/api/community/user-question",
    "title": "User Questions",
    "name": "Get_UserQuestionsListing",
    "group": "My_Forum",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "limit",
            "description": "<p>set to limit the filter results</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "offset",
            "description": "<p>set to offset the filter results to a particular record count</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "keyword",
            "description": "<p>pass entered search key with query string</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "     HTTP/1.1 2000 OK\n       {\n   \"status_code\":\"2000\",\n   \"message\":\"success\",\n   \"body\":{\n      \"count\":20,\n      \"questions\":[\n         {\n            \"topic_id\":14,\n            \"question_id\":226,\n            \"question\":\"test\",\n            \"topic_name\":\"Registration Know-how\",\n            \"publish_date\":\"2020-01-30 12:22:28\",\n            \"likes\":0,\n            \"dislikes\":0,\n            \"count_comments\":1,\n            \"description\":\"test\",\n            \"slug\":\"test-4\",\n            \"first_name\":\"Wep\",\n            \"last_name\":\"Niti Aayog\",\n            \"user_id\":\"wep-niti-aayog\",\n            \"user_type\":\"super-admin\",\n            \"profile_pic\":\"\",\n            \"entreprise_name\":\"\"\n         },\n        \n         {\n            \"topic_id\":17,\n            \"question_id\":212,\n            \"question\":\"duplicate aerty \",\n            \"topic_name\":\"aertry12\",\n            \"publish_date\":\"2020-01-01 16:19:23\",\n            \"likes\":0,\n            \"dislikes\":4,\n            \"count_comments\":1,\n            \"description\":\"duplicate aerty \",\n            \"slug\":\"duplicate-aerty\",\n            \"first_name\":\"Wep\",\n            \"last_name\":\"Niti Aayog\",\n            \"user_id\":\"wep-niti-aayog\",\n            \"user_type\":\"super-admin\",\n            \"profile_pic\":\"\",\n            \"entreprise_name\":\"\"\n         }\n      ]\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "My_Forum"
  },
  {
    "type": "PATCH",
    "url": "{base_url}/api/community/question/{slug}",
    "title": "Update",
    "name": "Update_my_forum_question",
    "group": "My_Forum",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>update description</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "forum_question",
            "description": "<p>update forum question</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "topic_id",
            "description": "<p>id for updating question</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "slug",
            "description": "<p>question slug</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n  {\n     \"status_code\":\"2000\",\n     \"message\":\"success\",\n     \"body\":[\n\n     ]\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "My_Forum"
  },
  {
    "type": "get",
    "url": "/category-question/search/{slug}/{search}",
    "title": "search question based on category",
    "name": "getCategoryQuestionSearch",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "slug",
            "optional": false,
            "field": "slug",
            "description": "<p>of category</p>"
          },
          {
            "group": "Parameter",
            "type": "search",
            "optional": false,
            "field": "parameter",
            "description": "<p>to be search</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n  {\n    \"status_code\": \"2000\",\n   \"message\": \"success\",\n   \"body\": {\n   \"count\": 1,\n   \"questions\": [\n       {\n           \"question_id\": 26,\n           \"question\": \"tax\",\n           \"topic_name\": \"Technology Validation\",\n           \"publish_date\": \"2019-03-06 07:34:58\",\n           \"likes\": 0,\n           \"dislikes\": 0,\n           \"count_comments\": 0,\n           \"description\": \"....\",\n           \"slug\": \"tax-3\",\n           \"first_name\": \"WEP ADMIN\",\n           \"last_name\": \"\",\n           \"user_id\": \"ashutosh\",\n           \"user_type\": \"super-admin\",\n           \"profile_pic\": \"admin-pic.png\",\n           \"entreprise_name\": \"\"\n       }\n   ],\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "/category-question/{slug}?keyword=",
    "title": "get specific question based on category and pass value with query string with keyword",
    "name": "getCategoryWiseQuestionsListing",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "slug",
            "optional": false,
            "field": "category",
            "description": "<p>slug</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n  {\n    \"status_code\": \"2000\",\n   \"message\": \"success\",\n   \"body\": {\n    \"count\": 1,\n   \"questions\": [\n       {\n           \"question_id\": 26,\n           \"question\": \"tax\",\n           \"topic_name\": \"Technology Validation\",\n           \"publish_date\": \"2019-03-06 07:34:58\",\n           \"likes\": 0,\n           \"dislikes\": 0,\n           \"count_comments\": 0,\n           \"description\": \"....\",\n           \"slug\": \"tax-3\",\n           \"first_name\": \"WEP ADMIN\",\n           \"last_name\": \"\",\n           \"user_id\": \"ashutosh\",\n           \"user_type\": \"super-admin\",\n           \"profile_pic\": \"admin-pic.png\",\n           \"entreprise_name\": \"\"\n       }\n   ],\n    }",
          "type": "json"
        },
        {
          "title": "Success-Response-2000: Search result",
          "content": "  {\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"count\": 3,\n        \"questions\": [\n            {\n                \"question_id\": 159,\n                \"question\": \"test q2\",\n                \"topic_name\": \"Incubation Support\",\n                \"publish_date\": \"2019-05-14 11:34:45\",\n                \"likes\": 0,\n                \"dislikes\": 0,\n                \"count_comments\": 8,\n                \"description\": \"test q2\",\n                \"slug\": \"test-q2\",\n                \"first_name\": \"rupesh\",\n                \"last_name\": \"\",\n                \"user_id\": \"ashutosh\",\n                \"user_type\": \"super-admin\",\n                \"profile_pic\": \"Ashutosh.jpg\",\n                \"entreprise_name\": \"\"\n            },\n            {\n                \"question_id\": 159,\n                \"question\": \"test q2\",\n                \"topic_name\": \"Incubation Support\",\n                \"publish_date\": \"2019-05-14 11:34:45\",\n                \"likes\": 0,\n                \"dislikes\": 0,\n                \"count_comments\": 8,\n                \"description\": \"test q2\",\n                \"slug\": \"test-q2\",\n                \"first_name\": \"rupesh\",\n                \"last_name\": \"\",\n                \"user_id\": \"ashutosh\",\n                \"user_type\": \"super-admin\",\n                \"profile_pic\": \"\",\n                \"entreprise_name\": \"\"\n            },\n            {\n                \"question_id\": 91,\n                \"question\": \"Testing 240\",\n                \"topic_name\": \"Incubation Support\",\n                \"publish_date\": \"2019-03-29 19:37:20\",\n                \"likes\": 1,\n                \"dislikes\": 0,\n                \"count_comments\": 4,\n                \"description\": \"testing 240 description\",\n                \"slug\": \"testing-240\",\n                \"first_name\": \"Kailash\",\n                \"last_name\": \"Panigrahi\",\n                \"user_id\": \"kailash-panigrahi\",\n                \"user_type\": \"super-admin\",\n                \"profile_pic\": \"1551352290fd456406745d816a45cae554c788e754.png\",\n                \"entreprise_name\": \"\"\n            }\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "/user-question/{slug}",
    "title": "get user  specific question",
    "name": "getUserQuestionSearch",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "slug",
            "optional": false,
            "field": "search",
            "description": "<p>parameter</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n  {\n    \"status_code\": \"2000\",\n   \"message\": \"success\",\n   \"body\": {\n   \"count\": 1,\n   \"questions\": [\n       {\n            \"question_id\": 26,\n           \"question\": \"tax\",\n           \"topic_name\": \"Technology Validation\",\n           \"publish_date\": \"2019-03-06 07:34:58\",\n           \"likes\": 0,\n           \"dislikes\": 0,\n           \"count_comments\": 0,\n           \"description\": \"....\",\n           \"slug\": \"tax-3\",\n           \"first_name\": \"WEP ADMIN\",\n           \"last_name\": \"\",\n           \"user_id\": \"ashutosh\",\n           \"user_type\": \"super-admin\",\n           \"profile_pic\": \"admin-pic.png\",\n           \"entreprise_name\": \"\"\n       }\n   ],\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "/thread-answer",
    "title": "get specific question answer and thread",
    "name": "threadAnswerList",
    "group": "admin",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n  {\n    \"status_code\": \"2000\",\n   \"message\": \"success\",\n   \"body\": {\n    \"count\": 1,\n   \"questions\": [\n       {\n           \"question_id\": 26,\n           \"question\": \"tax\",\n           \"topic_name\": \"Technology Validation\",\n           \"publish_date\": \"2019-03-06 07:34:58\",\n           \"likes\": 0,\n           \"dislikes\": 0,\n           \"count_comments\": 0,\n           \"description\": \"....\",\n           \"slug\": \"tax-3\",\n           \"first_name\": \"WEP ADMIN\",\n           \"last_name\": \"\",\n           \"user_id\": \"ashutosh\",\n           \"user_type\": \"super-admin\",\n           \"profile_pic\": \"admin-pic.png\",\n           \"entreprise_name\": \"\"\n       }\n   ],\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ForumController.php",
    "groupTitle": "admin"
  }
] });

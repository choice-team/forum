<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThreadIdToUserForumLikeMapperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_forum_like_mapper', function (Blueprint $table) {
            $table->integer('thread_id')->unsigned()->nullable(true);
            $table->integer('question_id')->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_forum_like_mapper', function (Blueprint $table) {
            //
        });
    }
}

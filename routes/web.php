<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('api/community/search/{search_category}/{search_keyword}','ExampleController@globalSearch');
$router->post('api/community/search','ExampleController@search');
//Recommendation
$router->post('api/community/liked-forum','ForumRecommendationController@likedForum');
$router->post('api/community/unliked-forum','ForumRecommendationController@unLikedForum');

$router->post('api/community/forum-recommendation-id','ForumRecommendationController@getForumRecommendedId');
$router->post('/api/community/get-user-liked-forum', 'ForumRecommendationController@getLikedForums');




$router->group(['prefix'=>'api/community', 'middleware' => 'auth:api'], function() use ($router){
   
    //Forum
    $router->get('/forum', 'ForumController@getForumListing');
    $router->get('/forum/{id}', 'ForumController@viewForum');
    $router->post('/forum', 'ForumController@createForum');
    $router->patch('/forum/{id}', 'ForumController@updateForum');
    $router->delete('/forum/{id}', 'ForumController@deleteForum');

    //Topics

    $router->get('/topic', 'ForumController@getTopicListing');
    $router->get('/topic/{id}', 'ForumController@viewTopic');
    $router->post('/topic', 'ForumController@createTopic');
    $router->patch('/topic/{id}', 'ForumController@updateTopic');
    $router->delete('/topic/{id}', 'ForumController@deleteTopic');


    //Questions
    $router->get('/question', 'ForumController@getQuestionsListing');
    $router->get('/question/{slug}', 'ForumController@viewQuestions');
    $router->post('/question', 'ForumController@createQuestions');
    $router->patch('/question/{slug}', 'ForumController@updateQuestions');
    $router->patch('/delete-question/{slug}', 'ForumController@deleteQuestions');

    $router->post('/question/like', 'ForumController@addLikesQuestion');


    //Threads
    $router->get('/thread', 'ForumController@getThreadListing');
    $router->get('/thread/{slug}', 'ForumController@viewThread');
    $router->post('/thread', 'ForumController@createThread');
    $router->patch('/thread/{id}', 'ForumController@updateThread');
    $router->patch('/delete-thread/{id}', 'ForumController@deleteThread');
    $router->post('/thread/like', 'ForumController@addLikesThread');


    //Top Contributor List
    $router->get('/top-contributor', 'ForumController@topContributor');
    $router->post('/likestatus', 'ForumController@updateLikeStatus');
    $router->get('/current-time','ForumController@getTime');

    // //script to generate question_slug
    // $router->get('/generate-forum-slug', 'ForumController@generateForumSlug');
    //route for starting a thread against a answer
    $router->post('/thread-answer','ForumController@threadAnswer');
    //route of the listing of the answers against a threa
    $router->post('/list-against-answer','ForumController@threadAnswerList');

    $router->get('/user-question', 'ForumController@getUserQuestionsListing');
    $router->get('/user-question/search/{slug}', 'ForumController@getUserQuestionSearch');

    $router->get('/category-question/{slug}', 'ForumController@getCategoryWiseQuestionsListing');
    $router->get('/category-question/search/{slug}/{search}', 'ForumController@getCategoryQuestionSearch');
    $router->get('/add-module','ExampleController@addModule');

    $router->get('/search/{search}','ForumController@searchCommunity');
    $router->post('/userfeed-forum','ForumController@getUserFeed');
    //$router->get('userwise-question-answer','ForumController@getForumQuestionAnswerUserWise');
    $router->get('userwise-question-answer','ForumController@myComments');



    

} );


